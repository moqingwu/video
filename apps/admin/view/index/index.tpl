{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("admin/index/index")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  {:lang("admin/index/index")}
</h6>
{:DcHookListen('admin_index_header',$params)}
<div class="alert alert-secondary mb-2" role="alert">
  <strong>{$user.user_name|DcHtml}</strong> {:lang('welcome')}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<!--数据统计-->
<div class="row row-1 row-cols-1 row-cols-md-3 row-cols-lg-4 row-cols-xl-6">
{volist name="datas" id="admin"}
<div class="col mb-2 px-1">
  <div class="card h-100">
    <div class="card-body py-3 d-flex flex-row align-items-center">
      <i class="fa fa-fw fa-2x mr-3 {$admin.ico|default='fa-check'} {$admin.color|default='text-danger'}"></i>
      <div>
        <h5 class="card-title mb-1" id="{$admin.id}">{$admin.count|number_format}</h5>
        <p class="card-text text-muted mb-0">{:lang($admin['title'])}</p>
      </div>
    </div>
  </div>
</div>
{/volist}
</div>
<!--应用通知-->
{$notice}
<!--服务器信息-->
<div class="card">
  <ul class="list-group list-group-flush">
    {if config('common.apply_name')}
    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 col-xl-1 mb-2 mb-md-0">{:lang('apply_version')}</div>
        <div class="col-md-10 col-xl-11">{:config('common.apply_version')} <span class="fa fa-spinner fa-spin dc-version" data-toggle="version" data-version="{:config('common.apply_version')}" data-module="{:config('common.apply_module')}"></span></div>
      </div>
    </li>
    {/if}
    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 col-xl-1 mb-2 mb-md-0">{:lang('frame_version')}</div>
        <div class="col-md-10 col-xl-11">{:config('daicuo.version')} <span class="fa fa-spinner fa-spin dc-version" data-toggle="version" data-version="{:config('daicuo.version')}" data-module="daicuo"></span></div>
      </div>
    </li>
    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 col-xl-1 mb-2 mb-md-0">{:lang('frame_author')}</div>
        <div class="col-md-10 col-xl-11"><a class="text-dark" href="mailto:{:lang('appAuthor')}">{:lang('appAuthor')}</a></div>
      </div>
    </li>
    {if in_array('administrator',$user['user_capabilities'])}
    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 col-xl-1 mb-2 mb-md-0">{:lang('php_version')}</div>
        <div class="col-md-10 col-xl-11">{$Think.PHP_VERSION}</div>
      </div>
    </li>
    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 col-xl-1 mb-2 mb-md-0">{:lang('php_engine')}</div>
        <div class="col-md-10 col-xl-11">{$Think.PHP_SAPI}</div>
      </div>
    </li>
    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 col-xl-1 mb-2 mb-md-0">{:lang('database_type')}</div>
        <div class="col-md-10 col-xl-11">{:config('database.type')}</div>
      </div>
    </li>
    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 col-xl-1 mb-2 mb-md-0">{:lang('web_directory')}</div>
        <div class="col-md-10 col-xl-11">{$path_root}</div>
      </div>
    </li>
    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 col-xl-1 mb-2 mb-md-0">{:lang('physical_path')}</div>
        <div class="col-md-10 col-xl-11">{:input('server.document_root')}</div>
      </div>
    </li>
    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 col-xl-1 mb-2 mb-md-0">{:lang('server_environment')}</div>
        <div class="col-md-10 col-xl-11">{$Think.PHP_OS} {:input('server.server_software')}</div>
      </div>
    </li>
    {/if}
  </ul>
</div>
{:DcHookListen('admin_index_footer',$params)}
{/block}