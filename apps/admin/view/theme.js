//jquery初始化脚本方法、事件监听
$(function() {
    daicuo.admin.init();
    daicuo.ajax.init();
    daicuo.form.init();
    daicuo.json.init();
    daicuo.tags.init();
    daicuo.upload.init();
});