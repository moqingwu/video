<?php
namespace app\admin\controller;

use app\admin\controller\Admin;

class Index extends Admin
{
    public function index()
    {
        //应用通知
        if(config('common.apply_name')){
            $url = [];
            $url['module']  = config('common.apply_module');
            $url['version'] = config('common.apply_version');
            $url['daicuo']  = config('daicuo.version');
            $url['host']    = input('server.HTTP_HOST/s','127.0.0.1');
            $this->assign('notice', DcCurl('auto','2',\daicuo\Service::apiUrl().'/welcome/?'.http_build_query($url)));
        }
        
        //数据统计
        $datas = [];
    
        //统计钩子
        \think\Hook::listen('admin_index_count', $datas);
        
        $this->assign('datas', $datas);
        
        return $this->fetch();
    }

    public function login()
    {
        if( $this->request->isPost() ){
        
            if(captcha_check(input('post.user_captcha')) == false){
                $this->error(lang('user_captcha_error'));
            }
            
            if(\daicuo\User::login(input('post.')) == false){
                $this->error(\daicuo\User::getError());
            }
            
            if( $this->request->isAjax() ){
                $this->success(lang('success'), 'index/index');
            }else{
                $this->redirect('index/index', '', '');
            }
        }
        
        if($this->site['user']['user_id']){
            $this->redirect('index/index', '', '');
        }
        
        $service = new \daicuo\Service();
        
        $this->assign('api_url', $service->apiUrl());
        
        return $this->fetch();
    }

    public function logout()
    {
        \daicuo\User::logout();
        
        $this->success(lang('logout').lang('success'), 'index/login');
    }
}