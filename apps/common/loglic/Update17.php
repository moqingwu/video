<?php
namespace app\common\loglic;

use app\common\loglic\Update;

class Update17 extends Update
{
    private $version = '1.7.28';
    
    public function upgrade()
    {
        if( is_file('./datas/db/'.$this->version.'.lock') ){
			$this->error = '已经是最新版，建议删除该文件（./apps/common/loglic/Update17.php）';
            return false;
		}
        
        //升级表结构	
		if(config('database.type') == 'sqlite'){
            $this->_sqlite();
        }else{
            $this->_mysql();
        }
        
        //增加锁文件
        touch('./datas/db/'.$this->version.'.lock');
        
        //删除旧文件
        $this->deleteFiles();
        
        return true;
    }
    
    //mysql脚本
    private function _mysql()
    {
        
        $prefix = config('database.prefix');
        
        $sql = [];
        
        array_push($sql, "ALTER TABLE ".$prefix."term Add term_title VARCHAR(255) NULL;");
        
        array_push($sql, "ALTER TABLE ".$prefix."term Add term_keywords VARCHAR(255) NULL;");
        
        array_push($sql, "ALTER TABLE ".$prefix."term Add term_description VARCHAR(255) NULL;");
        
        array_push($sql, "ALTER TABLE ".$prefix."info Add info_keywords VARCHAR(255) NULL;");
        
        array_push($sql, "ALTER TABLE ".$prefix."info Add info_description VARCHAR(255) NULL;");
        
        array_push($sql,"DROP TABLE IF EXISTS `".$prefix."log`;");
        
        array_push($sql, "CREATE TABLE `".$prefix."log` (
          `log_id` bigint(20) NOT NULL,
          `log_user_id` bigint(20) NOT NULL DEFAULT '0',
          `log_info_id` bigint(20) NOT NULL DEFAULT '0',
          `log_value` int(11) NOT NULL DEFAULT '0' COMMENT '整数值',
          `log_decimal` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT '小数值',
          `log_status` varchar(60) NOT NULL DEFAULT 'normal',
          `log_module` varchar(150) DEFAULT NULL,
          `log_controll` varchar(150) DEFAULT NULL,
          `log_action` varchar(150) DEFAULT NULL,
          `log_type` varchar(100) DEFAULT NULL,
          `log_ip` varchar(250) DEFAULT NULL,
          `log_name` varchar(250) DEFAULT NULL,
          `log_info` tinytext,
          `log_create_time` int(11) NOT NULL
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
        //索引
        array_push($sql, "ALTER TABLE `".$prefix."log`
          ADD PRIMARY KEY (`log_id`),
          ADD KEY `log_user_id` (`log_user_id`),
          ADD KEY `log_info_id` (`log_info_id`),
          ADD KEY `log_status` (`log_status`),
          ADD KEY `log_module` (`log_module`),
          ADD KEY `log_controll` (`log_controll`),
          ADD KEY `log_action` (`log_action`),
          ADD KEY `log_type` (`log_type`);");
        //自增
        array_push($sql, "ALTER TABLE `".$prefix."log`
          MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT;");
        //执行SQL语句
        $this->executeSql($sql);
    }
    
    //sqlite3数据库
    private function _sqlite()
    {
        //定义变量
        $sql = [];
        
        array_push($sql, "ALTER TABLE dc_term Add term_title VARCHAR(255) NULL;");
        
        array_push($sql, "ALTER TABLE dc_term Add term_keywords VARCHAR(255) NULL;");
        
        array_push($sql, "ALTER TABLE dc_term Add term_description VARCHAR(255) NULL;");
        
        array_push($sql, "ALTER TABLE dc_info Add info_keywords VARCHAR(255) NULL;");
        
        array_push($sql, "ALTER TABLE dc_info Add info_description VARCHAR(255) NULL;");
        
        array_push($sql, "DROP TABLE IF EXISTS dc_log;");
        
        array_push($sql, "CREATE TABLE [dc_log] (
            [log_id] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,
            [log_user_id] INTEGER DEFAULT '0' NULL,
            [log_info_id] INTEGER DEFAULT '0' NULL,
            [log_value] INTEGER DEFAULT '0' NULL,
            [log_decimal] REAL DEFAULT '0.00' NULL,
            [log_status] VARCHAR(100) DEFAULT 'normal' NULL,
            [log_module] VARCHAR(100) DEFAULT 'common' NULL,
            [log_controll] VARCHAR(100)  NULL,
            [log_action] VARCHAR(100)  NULL,
            [log_type] VARCHAR(100)  NULL,
            [log_ip] VARCHAR(100)  NULL,
            [log_name] VARCHAR(255)  NULL,
            [log_info] TEXT  NULL,
            [log_create_time] INTEGER DEFAULT '0' NULL
        );");
        
        array_push($sql,"CREATE INDEX [log_id] ON [dc_log](log_id);");
        
        array_push($sql,"CREATE INDEX [log_user_id] ON [dc_log](log_user_id);");
        
        array_push($sql,"CREATE INDEX [log_info_id] ON [dc_log](log_info_id);");
        
        array_push($sql,"CREATE INDEX [log_status] ON [dc_log](log_status);");
        
        array_push($sql,"CREATE INDEX [log_module] ON [dc_log](log_module);");
        
        array_push($sql,"CREATE INDEX [log_controll] ON [dc_log](log_controll);");
        
        array_push($sql,"CREATE INDEX [log_action] ON [dc_log](log_action);");
        
        array_push($sql,"CREATE INDEX [log_type] ON [dc_log](log_type);");
        
        //执行SQL语句
        $this->executeSql($sql);
    }
    
    //删除旧文件
    private function deleteFiles()
    {
        $file = new \files\File();
        $file->f_delete('./apps/sql.php');
        $file->f_delete('./extend/daicuo/category.php');
        $file->f_delete('./extend/daicuo/tag.php');
    }
}