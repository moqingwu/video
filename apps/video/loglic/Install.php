<?php
namespace app\video\loglic;

class Install
{
    //一键安装回调
    public function mysql()
    {
        \daicuo\Apply::install('video','install');
        
        \daicuo\Apply::install('friend','install');
        
        \daicuo\Apply::install('adsense','install');
        
        model('video/Datas','loglic')->updatePack();
        
        $this->routeIndex('video/index/index','video');
        
        return true;
    }
    
    //设置首页路由
    private function routeIndex($address='index/index/index', $module='video')
    {
        //公共
        \daicuo\Op::write([
            'site_name'  => '呆错短视频系统',
            'url_suffix' => '.html',
        ], 'common', 'config', 'system', 0, 'yes');
        //配置
        \daicuo\Op::write([
            'rewrite_index' => '/',
        ], 'video', 'config', 'system', 0, 'yes');
        //删除
        \daicuo\Op::delete_all([
            'op_name'   => ['eq','site_route'],
            'op_module' => ['eq','video'],
        ]);
        //新增
        return \daicuo\Route::save([
            'rule'        => '/',
            'address'     => $address,
            'method'      => '*',
            'op_module'   => $module,
        ]);
    }
}