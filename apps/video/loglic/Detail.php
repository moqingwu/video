<?php
namespace app\video\loglic;

class Detail
{
    public function fields($data=[])
    {
        $fields = [
            'info_action' => [
                'order'           => 0,
                'type'            => 'hidden',
                'value'           => $data['info_action'],
            ],
            'info_user_id' => [
                'order'           => 0,
                'type'            => 'hidden',
                'value'           => DcUserCurrentGetId(),
            ],
            'info_id' => [
                'order'           => 2,
                'type'            => 'hidden',
                'value'           => $data['info_id'],
                'data-title'      => lang('video_id'),
                'data-filter'     => false,
                'data-visible'    => true,
                'data-sortable'   => true,
                'data-width'      => '80',
                'data-width-unit' => 'px',
            ],
            'html_1' => [
                'order'           => 1,
                'type'            => 'html',
                'value'           => '<div class="form-row">',
            ],
            'info_status_text' => [
                'order'           => 14,
                'data-title'      => lang('video_status'),
                'data-visible'    => true,
                'data-width'      => 80,
            ],
            'info_name' => [
                'order'           => 3,
                'type'            => 'text',
                'value'           => $data['info_name'],
                'title'           => lang('video_name'),
                'class'           => 'form-group col-md-6',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'data-filter'     => false,
                'data-visible'    => true,
                'data-align'      => 'left',
                'data-class'      => 'text-wrap',
            ],
            'video_category' => [
                'order'           => 6,
                'data-title'      => lang('video_category'),
                'data-visible'    => true,
            ],
            'info_excerpt' => [
                'order'           => 9,
                'type'            => 'file',
                'value'           => $data['info_excerpt'],
                'title'           => lang('video_excerpt'),
                'class'           => 'form-group col-md-6',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'info_slug' => [
                'order'           => 4,
                'type'            => 'text',
                'value'           => $data['info_slug'],
                'title'           => lang('video_slug'),
                'class'           => 'form-group col-md-6',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'video_cover' => [
                'order'           => 5,
                'type'            => 'image',
                'value'           => $data['video_cover'],
                'class'           => 'form-group col-md-6',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_cover'),
                'placeholder'     => '',
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'tag_name'  => [
                'order'           => 7,
                'type'            =>'tags',
                'value'           => implode(',',$data['tag_name']),
                //'option'          => videoTags(20),
                'class'           => 'form-group col-md-6',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_tag'),
                'class_tags'      => 'form-text pt-1',
                'class_tags_list' => 'text-purple mr-2',
            ],
            'video_image' => [
                'order'           => 6,
                'type'            => 'image',
                'value'           => $data['video_image'],
                'class'           => 'form-group col-md-6',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_image'),
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'info_title' => [
                'order'           => 25,
                'type'            => 'text',
                'value'           => $data['info_title'],
                'class'           => 'form-group col-md-6',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_title'),
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'info_keywords' => [
                'order'           => 26,
                'type'            => 'text',
                'value'           => $data['info_keywords'],
                'class'           => 'form-group col-md-6',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_keywords'),
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'category_id' => [
                'order'             => 12,
                'type'              => 'select',
                'value'             => $data['category_id'],
                'option'            => DcTermCheck(['module'=>['eq','video'],'action'=>['eq','index']]),
                'multiple'          => true,
                'size'              => 5,
                'class'             => 'form-group col-md-3',
                'class_left'        => 'w-100',
                'class_right'       => 'w-100 py-1',
                'title'             => lang('video_category'),
                'data-filter'       => true,
                'data-visible'      => false,
                'data-type'         => 'select',
                'data-option'       => $this->categoryOption(), 
            ],
            'info_description' => [
                'order'           => 27,
                'type'            => 'textarea',
                'value'           => $data['info_description'],
                'rows'            => 5,
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_description'),
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'info_content' => [
                'order'           => 8,
                'type'            => 'textarea',
                'value'           => $data['info_content'],
                'title'           => lang('video_content'),
                'rows'            => 5,
                'class'           => 'form-group col-md-6',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'info_type' => [
                'order'           => 15,
                'type'            => 'select',
                'value'           => DcEmpty($data['info_type'],'index'),
                'option'          => $this->type(),
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_type'),
                'data-title'      => lang('video_type'),
                'data-filter'     => true,
                'data-visible'    => true,
                'data-width'      => 80,
            ],
            'info_status' => [
                'order'           => 13,
                'type'            => 'select',
                'value'           => DcEmpty($data['info_status'],'normal'),
                'option'          => $this->status(),
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_status'),
                'data-filter'     => true,
                'data-visible'    => false,
            ],
            'video_head' => [
                'order'           => 13,
                'type'            => 'select',
                'value'           => $data['video_head'],
                'option'          => $this->isOff(),
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_head'),
                'data-filter'     => true,
                'data-visible'    => false,
                'data-option'     => videoDataOption($this->isOff()), 
            ],
            'video_hot' => [
                'order'           => 13,
                'type'            => 'select',
                'value'           => $data['video_hot'],
                'option'          => $this->isOff(),
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_hot'),
                'data-filter'     => true,
                'data-visible'    => false,
                'data-option'     => videoDataOption($this->isOff()), 
            ],
            'info_views' => [
                'order'           => 18,
                'type'            => 'number',
                'value'           => intval($data['info_views']),
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'data-filter'     => false,
                'data-visible'    => true,
                'data-sortable'   => true,
                'data-width'      => 80,
            ],
            'info_hits' => [
                'order'           => 19,
                'type'            => 'number',
                'value'           => intval($data['info_hits']),
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'data-filter'     => false,
                'data-visible'    => true,
                'data-sortable'   => true,
                'data-width'      => 80,
            ],
            'video_up' => [
                'order'           => 20,
                'type'            => 'number',
                'value'           => intval($data['video_up']),
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_up'),
                'data-title'      => lang('video_up'),
                'data-filter'     => false,
                'data-visible'    => true,
                'data-sortable'   => true,
                'data-width'      => 80,
            ],
            'video_down' => [
                'order'           => 21,
                'type'            => 'number',
                'value'           => intval($data['video_down']),
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_down'),
                'data-title'      => lang('video_down'),
                'data-filter'     => false,
                'data-visible'    => true,
                'data-sortable'   => true,
                'data-width'      => 80,
            ],
            'info_order' => [
                'order'           => 17,
                'type'            => 'number',
                'value'           => intval($data['info_order']),
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'data-filter'     => false,
                'data-visible'    => true,
                'data-sortable'   => true,
                'data-width'      => 80,
            ],
            'video_letter' => [
                'order'           => 22,
                'type'            => 'text',
                'value'           => $data['video_letter'],
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_letter'),
                'data-title'      => lang('video_letter'),
                'data-filter'     => false,
                'data-visible'    => true,
                'data-sortable'   => true,
                'data-width'      => 80,
            ],
            'video_tpl' => [
                'order'           => 23,
                'type'            => 'text',
                'value'           => $data['video_tpl'],
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_tpl'),
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'video_referer' => [
                'order'           => 24,
                'type'            => 'text',
                'value'           => $data['video_referer'],
                'class'           => 'form-group col-md-3',
                'class_left'      => 'w-100',
                'class_right'     => 'w-100',
                'title'           => lang('video_referer'),
                'data-filter'     => false,
                'data-visible'    => false,
            ],
            'html_4' => [
                'order'           => 10,
                'type'            => 'html',
                'value'           => '</div>',
            ],
            'customs' => [
                'order'           => 10,
                'type'            => 'hidden',
                'value'           => '',
            ],
            'info_update_time' => [
                'order'           => 510,
                'data-visible'    => true,
                'data-sortable'   => true,
                'data-width'      => 120,
            ],
            
        ];
        //动态扩展字段
        $customs = videoMetaList('detail', DcEmpty($data['info_action'],'index'));
        //合并所有字段
        if($customs){
            $fields = DcArrayPush($fields, DcFields($customs, $data), 'customs');
        }
        //返回所有表单字段
        return $fields;
    }
    
    public function type()
    {
        //内型初始值
        $types = [];
        $types['index']    = lang('video_type_index');
        $types['vertical'] = lang('video_type_vertical');
        //折分内型扩展
        foreach(explode(',',config('video.type_option')) as $value){
            if($value){
                list($key, $lang) = explode('=>', $value);
                $types[$key] = lang(DcEmpty($lang, 'video_type_'.$key));
            }
        }
        //返回数据
        return $types;
    }
    
    public function status()
    {
        return [
            'normal'  => lang('normal'),
            'hidden'  => lang('hidden'),
            'private' => lang('private'),
        ];
    }
    
    public function isOff()
    {
        return [
            0  => lang('off'),
            1  => lang('on'),
        ];
    }
    
    public function categoryOption()
    {
        $options = DcTermCheck(['module'=>['eq','video']]);
        $options[0] = '---';
        return $options; 
    }
}