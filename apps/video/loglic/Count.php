<?php
namespace app\video\loglic;

class Count
{
    public function category()
    {
        return db('term')->where(['term_module'=>'video','term_controll'=>'category'])->count('term_id');
    }
    
    public function tag()
    {
        return db('term')->where(['term_module'=>'video','term_controll'=>'tag'])->count('term_id');
    }
    
    public function views()
    {
        return db('info')->where(['info_module'=>'video'])->sum('info_views');
    }
    
    public function detail()
    {
        return db('info')->where(['info_module'=>'video'])->count('info_id');
    }
}