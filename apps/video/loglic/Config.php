<?php
namespace app\video\loglic;

class Config
{
    public function fields($data=[])
    {
        $themes = DcThemeOption('video');
    
        $items  = DcFormItems([
            'theme' => [
                'type'   =>'select', 
                'value'  => config('video.theme'), 
                'option' => $themes,
            ],
            'theme_wap' => [
                'type'   => 'select',
                'value'  => config('video.theme_wap'),
                'option' => $themes,
            ],
            'meta_referer' => [
                'type'       => 'select',
                'value'      => config('video.meta_referer'),
                'option'     => [0=>lang('close'),1=>lang('open')],
            ],
            'slug_first' => [
                'type'       => 'select',
                'value'      => config('video.slug_first'),
                'option'     => [0=>lang('close'),1=>lang('open')],
            ],
            'request_max' => [
                'type'        => 'number',
                'value'       => intval(config('video.request_max')),
                'tips'        => lang('video_request_max_tips'),
            ],
            'baidu_push' => [
                'type'        => 'text',
                'value'       => config('video.baidu_push'),
                'tips'        => lang('video_baidu_push_tips'),
            ],
            'post_pwd' => [
                'type'       => 'text',
                'value'      => config('video.post_pwd'),
                'tips'        => lang('video_post_pwd_tips'),
            ],
            'search_list' => [
                'type'        => 'text',
                'value'       => config('video.search_list'),
                'rows'        => 3,
                'tips'        => lang('video_search_list_tips'),
            ],
            'search_hot' => [
                'type'        => 'text',
                'value'       => config('video.search_hot'),
                'rows'        => 3,
                'tips'        => lang('video_search_hot_tips'),
            ],
            'type_option' => [
                'type'       => 'text',
                'value'      => config('video.type_option'),
                'tips'        => lang('video_type_option_tips'),
            ],
            'hr_1' => [
                'type'        => 'html',
                'value'       => '<hr>',
            ],
            'limit_index' => [
                'type'        => 'number',
                'value'       => intval(config('video.limit_index')),
            ],
            'limit_category' => [
                'type'        => 'number',
                'value'       => DcEmpty(config('video.limit_category'),20),
            ],
            'limit_tag' => [
                'type'        => 'number',
                'value'       => DcEmpty(config('video.limit_tag'),20),
            ],
            'limit_search' => [
                'type'        => 'number',
                'value'       => DcEmpty(config('video.limit_search'),10),
            ],
            'limit_sitemap' => [
                'type'        => 'number',
                'value'       => DcEmpty(config('video.limit_sitemap'),10),
            ],
        ]);
        
        foreach($items as $key=>$value){
            $items[$key]['title'] = lang('video_'.$key);
            $items[$key]['placeholder'] = '';
        }
        
        return $items;
    }
}