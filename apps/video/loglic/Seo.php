<?php
namespace app\video\loglic;

class Seo
{
    public function fields($data=[])
    {
        $items = [
            'rewrite_index' => [
                'type'        => 'text', 
                'value'       => DcEmpty(config('video.rewrite_index'), 'video$'),
                'tips'        => 'video$',
            ],
            'rewrite_category' => [
                'type'        => 'text', 
                'value'       => config('video.rewrite_category'),
                'tips'        => '[:id] [:slug] [:name] [:pageNumber]',
            ],
            'rewrite_tag' => [
                'type'        => 'text',
                'value'       => config('video.rewrite_tag'),
                'tips'        => '[:id] [:slug] [:name] [:pageNumber]',
            ],
            'rewrite_search' => [
                'type'        => 'text',
                'value'       => config('video.rewrite_search'),
                'tips'        => '[:searchText] [:pageNumber]',
            ],
            'rewrite_detail' => [
                'type'        => 'text',
                'value'       => config('video.rewrite_detail'),
                'tips'        => '[:id] [:slug] [:name] [:termId] [:termSlug] [:termName]',
            ],
            'html_hr' => [
                'type'        => 'html',
                'value'       => '<hr>',
            ],
            'index_title' => [
                'type'        => 'text', 
                'value'       => config('video.index_title'),
                'tips'        => '[siteName] [siteDomain] [pageNumber]',
            ],
            'index_keywords' => [
                'type'        => 'text', 
                'value'       => config('video.index_keywords'),
                'tips'        => '[siteName] [siteDomain] [pageNumber]',
            ],
            'index_description' => [
                'type'        => 'text', 
                'value'       => config('video.index_description'),
                'tips'        => '[siteName] [siteDomain] [pageNumber]',
            ],
            'search_title' => [
                'type'        => 'text', 
                'value'       => config('video.search_title'),
                'tips'        => '[siteName] [siteDomain] [pageNumber] [searchText]',
            ],
            'search_keywords' => [
                'type'        => 'text', 
                'value'       => config('video.search_keywords'),
                'tips'        => '[siteName] [siteDomain] [pageNumber] [searchText]',
            ],
            'search_description' => [
                'type'        => 'text', 
                'value'       => config('video.search_description'),
                'tips'        => '[siteName] [siteDomain] [pageNumber] [searchText]',
            ],
        ];
        
        foreach($items as $key=>$value){
            $items[$key]['title']  = lang('video_'.$key);
        }
        
        return $items;
    }
}