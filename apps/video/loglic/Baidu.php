<?php
namespace app\video\loglic;

class Baidu
{
    public function fields($data=[])
    {
        return DcFormItems([
            'pageSize' => [
                'type'   => 'text', 
                'value'  => '100', 
                'title'  => '每页数量',
            ],
            'pageNumber' => [
                'type'   => 'text', 
                'value'  => DcEmpty(DcCache('baiduPushVideo'),1), 
                'title'  => '开始页码',
            ],
        ]);
    }
    
    public function fieldsForm($data=[])
    {
        return DcFormItems([
            'urls' => [
                'type'   => 'textarea', 
                'value'  => implode(chr(13),$data),
                'rows'   => '10',
                'title'  => '每页数量',
                'class_left'  => 'd-none',
                'class_right' => 'col-12',
            ],
        ]);
    }
}