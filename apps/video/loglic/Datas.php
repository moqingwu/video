<?php
namespace app\video\loglic;

class Datas
{
    //批量添加初始配置
    public function insertConfig()
    {
        config('common.validate_name', false);
        
        return model('common/Config','loglic')->install([
            'theme'             => 'default',
            'theme_wap'         => 'default',
            'index_title'       => '呆错短视频系统',
            'index_keywords'    => '呆错短视频系统,视频管理系统,videoCms,DaiCuoVideo',
            'index_description' => '呆错短视频系统是一款专业的免费视频管理系统，适合做短视频垂直细分内容平台。',
            'meta_referer'      => 1,
            'slug_first'        => 1,
            'request_max'       => 0,
            'search_hot'        => '',
            'search_action'     => '',
            'limit_index'       => 0,
            'limit_category'    => 30,
            'limit_tag'         => 60,
            'limit_search'      => 10,
            'limit_sitemap'     => 100,
            'rewrite_index'     => 'video$',
        ],'video');
    }
    
    //批量添加路由
    public function insertRoute()
    {
        config('common.validate_name', false);
        
        return model('common/Route','loglic')->install([
            [
                'rule'        => 'video$',
                'address'     => 'video/index/index',
                'method'      => '*',
                'op_module'   => 'video',
                'op_controll' => 'route',
                'op_action'   => 'system',
            ],
        ]);
    }
    
    //批量添加后台菜单
    public function insertMenu()
    {
        config('common.validate_name', false);
        
        $result = model('common/Menu','loglic')->install([
            [
                'term_name'   => '视频',
                'term_slug'   => 'video',
                'term_info'   => 'fa-file-video-o',
                'term_module' => 'video',
                'term_order'  => 9,
            ],
        ]);
        
        $result = model('common/Menu','loglic')->install([
            [
                'term_name'   => '视频管理',
                'term_slug'   => 'video/admin/index',
                'term_info'   => 'fa-navicon',
                'term_module' => 'video',
                'term_order'  => 9,
            ],
            [
                'term_name'   => '视频采集',
                'term_slug'   => 'video/collect/index',
                'term_info'   => 'fa-cloud',
                'term_module' => 'video',
                'term_order'  => 8,
            ],
            [
                'term_name'   => '栏目管理',
                'term_slug'   => 'admin/category/index?parent=video&term_module=video',
                'term_info'   => 'fa-list',
                'term_module' => 'video',
                'term_order'  => 7,
            ],
            [
                'term_name'   => '标签管理',
                'term_slug'   => 'admin/tag/index?parent=video&term_module=video',
                'term_info'   => 'fa-tags',
                'term_module' => 'video',
                'term_order'  => 6,
            ],
            [
                'term_name'   => 'SEO优化',
                'term_slug'   => 'video/seo/index',
                'term_info'   => 'fa-anchor',
                'term_module' => 'video',
                'term_order'  => 5,
            ],
            [
                'term_name'   => '字段扩展',
                'term_slug'   => 'admin/field/index?parent=video&op_module=video',
                'term_info'   => 'fa-cube',
                'term_module' => 'video',
                'term_order'  => 4,
            ],
            [
                'term_name'   => '百度推送',
                'term_slug'   => 'video/baidu/index',
                'term_info'   => 'fa-car',
                'term_module' => 'video',
                'term_order'  => 3,
            ],
            [
                'term_name'   => '频道设置',
                'term_slug'   => 'video/config/index',
                'term_info'   => 'fa-gear',
                'term_module' => 'video',
                'term_order'  => 2,
            ],
        ],'视频');
    }
    
    //批量添加分类/标签/导航
    public function insertTerm()
    {
        config('common.validate_name', false);
        
        //分类
        $result = model('common/Category','loglic')->install([
            [
                'term_name'       => '科学科普',
                'term_slug'       => 'kexuekepu',
                'term_type'       => 'navbar',
                'term_module'     => 'video',
            ],
            [
                'term_name'       => '法律法规',
                'term_slug'       => 'falvfagui',
                'term_type'       => 'navbar',
                'term_module'     => 'video',
            ],
            [
                'term_name'       => '人文历史',
                'term_slug'       => 'renwenlishi',
                'term_type'       => 'navbar',
                'term_module'     => 'video',
            ],
            [
                'term_name'       => '商业理财',
                'term_slug'       => 'shangyelicai',
                'term_type'       => 'navbar',
                'term_module'     => 'video',
            ],
            [
                'term_name'       => '职业职场',
                'term_slug'       => 'zhiyezhichang',
                'term_type'       => 'navbar',
                'term_module'     => 'video',
            ],
            [
                'term_name'       => '艺术兴趣',
                'term_slug'       => 'yishuxingqu',
                'term_type'       => 'navbar',
                'term_module'     => 'video',
            ],
            [
                'term_name'       => '情感心理',
                'term_slug'       => 'qingganxinli',
                'term_type'       => 'navbar',
                'term_module'     => 'video',
            ],
            [
                'term_name'       => '校园学习',
                'term_slug'       => 'xiaoyuanxuexi',
                'term_type'       => 'navbar',
                'term_module'     => 'video',
            ],
            [
                'term_name'       => '所有分类',
                'term_slug'       => 'types',
                'term_type'       => 'navbar',
                'term_module'     => 'video',
                'term_action'     => 'page',
                'term_tpl'        => 'types',
                'term_limit'      => 0,
            ],
            [
                'term_name'       => '标签列表',
                'term_slug'       => 'tags',
                'term_type'       => 'navbar',
                'term_module'     => 'video',
                'term_action'     => 'page',
                'term_tpl'        => 'tags',
                'term_limit'      => 0,
            ],
            [
                'term_name'       => '排行榜',
                'term_slug'       => 'views',
                'term_type'       => 'navbar',
                'term_module'     => 'video',
                'term_action'     => 'page',
                'term_tpl'        => 'views',
                'term_limit'      => 0,
            ],
        ]);
        
        //标签
        $result = model('common/Tag','loglic')->install([
            [
                'term_name'       => 'mp4',
                'term_slug'       => 'mp4',
                'term_module'     => 'video',
            ],
            [
                'term_name'       => 'm3u8',
                'term_slug'       => 'm3u8',
                'term_module'     => 'video',
            ],
        ]);
        
        //导航
        $result = model('common/Navs','loglic')->install([
            [
                'navs_name'       => '视频首页',
                'navs_url'        => 'video/index/index',
                'navs_status'     => 'normal',
                'navs_type'       => 'navbar',
                'navs_module'     => 'video',
                'navs_active'     => 'videoindexindex',
                'navs_target'     => '_self',
                'navs_order'      => 9,
            ],
            [
                'navs_name'       => '视频地图',
                'navs_url'        => 'video/sitemap/index',
                'navs_status'     => 'normal',
                'navs_type'       => 'link',
                'navs_module'     => 'video',
                'navs_active'     => 'videositemapindex',
                'navs_target'     => '_self',
                'navs_order'      => 9,
            ],
        ]);
        
        return true;
    }
    
    //批量添加扩展字段
    public function insertField()
    {
        config('common.validate_name', false);
        
        return model('common/Field','loglic')->install([
            [
                'op_name'     => 'video_cover',
                'op_value'    => json_encode([
                    'type'         => 'image',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'video',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'video_image',
                'op_value'    => json_encode([
                    'type'         => 'image',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'video',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'video_up',
                'op_value'    => json_encode([
                    'type'         => 'number',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'video',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'video_down',
                'op_value'    => json_encode([
                    'type'         => 'number',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'video',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'video_referer',
                'op_value'    => json_encode([
                    'type'         => 'text',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'video',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'video_letter',
                'op_value'    => json_encode([
                    'type'         => 'text',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'video',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'video_tpl',
                'op_value'    => json_encode([
                    'type'         => 'text',
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'video',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'video_head',
                'op_value'    => json_encode([
                    'type'         => 'select',
                    'option'       => [1=>lang('on'),0=>lang('off')],
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'video',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
            [
                'op_name'     => 'video_hot',
                'op_value'    => json_encode([
                    'type'         => 'select',
                    'option'       => [1=>lang('on'),0=>lang('off')],
                    'relation'     => 'eq',
                    'data-visible' => false,
                    'data-filter'  => false,
                ]),
                'op_module'   => 'video',
                'op_controll' => 'detail',
                'op_action'   => 'default',
            ],
        ]);
    }
    
    //添加采集规则
    public function insertCollect()
    {
        config('common.validate_name', false);
        
        return model('video/Collect','loglic')->write([
            'collect_name'     => '演示数据',
            'collect_url'      => 'http://demo.daicuo.com/index.php',
            'collect_token'    => 'fb05aabf582499263dfc235a82629069',
            'collect_category' => '',
        ]);
    }
    
    //添加测试数据
    public function insertDetail()
    {
        $data = [];
        $data['info_module']   = 'video';
        $data['info_controll'] = 'detail';
        $data['info_action']   = 'index';
        $data['info_staus']    = 'normal';
        $data['info_type']     = 'index';
        $data['info_user_id']  = 1;
        $data['info_order']    = 9;
        $data['info_name']     = '呆错短视频系统播放M3U8格式的演示视频';//名称
        $data['info_excerpt']  = 'https://hao.daicuo.cc/video/m3u8/demo.m3u8';//播放地址
        $data['info_content']  = '呆错短视频系统是一款专业的免费视频管理系统，适合做短视频垂直细分内容平台。';//详情
        $data['info_type']     = 'index';//形式(index|vertical)
        $data['term_id']       = model('common/Term','loglic')->nameToId('科学科普', 'video', 'category', 'yes');
        $data['term_id']       = DcArrayArgs(model('common/Term','loglic')->nameToId('daicuo,m3u8', 'video', 'tag', 'yes'), $data['term_id']);
        $data['video_cover']   = 'https://cdn.daicuo.cc/images/daicuo/640x380.png';//封面
        $data['video_up']      = 9;
        $data['video_down']    = 0;
        $data['video_head']    = 1;
        $data['video_hot']     = 1;
        $data['video_letter']  = 'd';
        //定义验证等
        config('common.validate_scene', false);
        config('common.where_slug_unique', ['info_module'=>['eq','video']]);
        config('custom_fields.info_meta', model('common/Info','loglic')->metaKeys('video','detail','index'));
        //返回结果
        return \daicuo\Info::save($data, 'info_meta,term_map');
    }
    
    //卸载时删除插件配置（不删除分类、标签、菜单、内容）
    public function remove()
    {
        //删除插件配置表
        \daicuo\Op::delete_module('video');
        
        //删除前台菜单
        model('common/Navs','loglic')->unInstall('video');
        
        //删除后台菜单
        model('common/Menu','loglic')->unInstall('video');
        
        return true;
    }
    
    //按插件应用名删除数据
    public function delete()
    {
        //删除插件配置表
        \daicuo\Op::delete_module('video');
        
        //删除队列表
        \daicuo\Term::delete_module('video');
        
        //删除内容表
        \daicuo\Info::delete_module('video');
        
        return true;
    }
    
    //更新应用状态
    public function updateStatus()
    {
        \daicuo\Apply::updateStatus('video', 'enable');
        
        return true;
    }
    
    //更新应用打包
    public function updatePack()
    {
        if(config('common.apply_module') == 'video'){
            \daicuo\Op::write([
                'apply_name'    => '呆错短视频系统',
                'apply_module'  => 'video',
                'apply_version' => '1.2.3',
                'apply_rely'    => '',
            ], 'common', 'config', 'system', 0, 'yes');
        }

        return true;
    }
    
    //更新应用字段
    public function updateField()
    {
        //db('op')->where(['op_module'=>'video','op_controll'=>'category','op_action'=>'page','op_autoload'=>'field'])->delete();
        
        return true;
    }
    
    //更新后台菜单
    public function updateMenu()
    {
        config('common.validate_name', false);
        
        return model('common/Menu','loglic')->install([
            [
                'term_name'   => '百度推送',
                'term_slug'   => 'video/baidu/index',
                'term_info'   => 'fa-car',
                'term_module' => 'video',
                'term_order'  => 4,
            ],
        ],'视频');
    }
    
}