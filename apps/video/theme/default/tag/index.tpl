{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords}" />
<meta name="description" content="{$seoDescription}" />
{if config('video.meta_referer')}<meta name="referrer" content="never">{/if}
{/block}
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
  <ol class="breadcrumb bg-white mb-3">
    <li class="breadcrumb-item"><a href="{:DcUrl('video/index/index')}">首页</a></li>
    <li class="breadcrumb-item"><a href="{:DcUrl('video/category/index',['slug'=>'tags'])}">标签</a></li>
    <li class="breadcrumb-item active">{$term_name|DcHtml}</li>
  </ol>
  <div class="row row-1 row-cols-2 row-cols-md-6">
   {volist name="data" id="video"}
    <div class="col px-2 mb-3">
      <a href="{:videoUrlDetail($video)}">
        <img class="w-100 rounded mb-2 img-fluid cover6_{$video.info_type}" src="{:videoUrlImage($video['video_cover'])}" alt="{$video.info_name|DcHtml}">
      </a>
      <a class="info_name" href="{:videoUrlDetail($video)}">{$video.info_name|videoSubstr=0,24}</a>
    </div>
  {/volist}
  </div>
  <!---->
  {gt name="last_page" value="1"}
  <div class="rounded bg-white pagesmall py-3 mb-2 d-md-none">{:DcPageSimple($current_page, $last_page, $pagePath)}</div>
  <div class="rounded bg-white page py-3 mb-2 d-none d-md-block">{:DcPage($page, $per_page, $total, $pagePath)}</div>
  {/gt}
</div>
{/block}
<!-- -->
{block name="footer"}{include file="widget/footer" /}{/block}