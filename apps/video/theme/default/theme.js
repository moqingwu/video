window.daicuo.video = {
    init: function(){
        this.searchDropdown();
        this.infoUpClick();
    },
    infoUpClick: function(){
        $(document).on("click", '[data-toggle="infoUp"]', function() {
            $(this).addClass('disabled');
            var btn = $(this).find('.infoUpValue');
            daicuo.ajax.get($(this).attr('href'), function($data, $status, $xhr) {
                if($data.code == 1){
                    btn.text($data.value);
                }
                
            });
            return false;
        });
    },
    searchDropdown: function(){
        $('#searchDropdown .dropdown-item').on('click', function(){
            var formAction = $('form[id="search"]').attr('action');
            var formText   = $('form [data-toggle="dropdown"]').text();
            var thisAction = $(this).data('href');
            var thisText   = $(this).text();
            //表单
            $('form[id="search"]').attr('action',thisAction);
            $('form [data-toggle="dropdown"]').text(thisText);
            //当前
            $(this).data('href',formAction);
            $(this).text(formText);
        });
    }
};
//主题JS
$(document).ready(function() { 
    //框架脚本
    window.daicuo.init();
    //主题脚本
    window.daicuo.video.init();
});