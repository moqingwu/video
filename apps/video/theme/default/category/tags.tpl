{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords}" />
<meta name="description" content="{$seoDescription}" />
{if config('video.meta_referer')}<meta name="referrer" content="never">{/if}
{/block}
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
  <ol class="breadcrumb bg-white mb-3">
    <li class="breadcrumb-item"><a href="{:DcUrl('video/index/index')}">首页</a></li>
    <li class="breadcrumb-item"><a href="{:DcUrl('video/category/index',['slug'=>'tags'])}">标签</a></li>
    <li class="breadcrumb-item active">{$term_name|DcHtml}</li>
  </ol>
  <div class="row row-1 row-cols-2 row-cols-md-6">
   {assign name="list" value=":videoTagSelect(['status'=>'normal','limit'=>60,'page'=>$pageNumber,'sort'=>'term_count desc,term_id','order'=>'desc'])" /}
   {volist name="list.data" id="tag"}
    <div class="col px-2 mb-3 text-center">
      <a class="btn btn-light btn-block py-3" href="{:videoUrlTag($tag)}">
        {$tag.term_name|videoSubstr=0,8} <sup class="text-purple">{$tag.term_count}</sup>
      </a>
    </div>
  {/volist}
  </div>
  <!---->
  {gt name="list.last_page" value="1"}
  <div class="rounded bg-white pagesmall py-3 mb-2 d-md-none">{:DcPageSimple($list['current_page'], $list['last_page'], $list['pagePath'])}</div>
  <div class="rounded bg-white page py-3 mb-2 d-none d-md-block">{:DcPage($pageNumber, $list['per_page'], $list['total'], $pagePath)}</div>
  {/gt}
</div>
{/block}
<!-- -->
{block name="footer"}{include file="widget/footer" /}{/block}