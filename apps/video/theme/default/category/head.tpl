{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords}" />
<meta name="description" content="{$seoDescription}" />
{if config('video.meta_referer')}<meta name="referrer" content="never">{/if}
{/block}
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
  <ol class="breadcrumb bg-white mb-3">
    <li class="breadcrumb-item"><a href="{:DcUrl('video/index/index')}">首页</a></li>
    <li class="breadcrumb-item active">{$term_name|DcHtml}</li>
  </ol>
  <ol class="bg-white mb-3 px-5 py-3">
  {volist name=":videoSelect(['status'=>'normal','limit'=>100,'sort'=>'info_create_time','order'=>'desc','meta_key'=>'video_head','meta_value'=>1])" id="video"}
    <li class="py-2"><a href="{:videoUrlDetail($video)}">{$video.info_name|DcHtml}</a> {$video.info_create_time|substr=0,10}</li>
  {/volist}
  </ol>
</div>
{/block}
<!-- -->
{block name="footer"}{include file="widget/footer" /}{/block}