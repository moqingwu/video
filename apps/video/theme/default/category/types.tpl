{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords}" />
<meta name="description" content="{$seoDescription}" />
{if config('video.meta_referer')}<meta name="referrer" content="never">{/if}
{/block}
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
  <ol class="breadcrumb bg-white mb-3">
    <li class="breadcrumb-item"><a href="{:DcUrl('video/index/index')}">首页</a></li>
    <li class="breadcrumb-item"><a href="{:DcUrl('video/category/index',['slug'=>'types'])}">分类</a></li>
    <li class="breadcrumb-item active">{$term_name|DcHtml}</li>
  </ol>
  <div class="row row-1 row-cols-2 row-cols-md-6">
  {volist name=":videoCategorySelect(['status'=>'normal','action'=>'index','sort'=>'term_count desc,term_id','order'=>'desc'])" id="category"}
    <div class="col px-2 mb-3 text-center">
      <a class="btn btn-light btn-block py-3" href="{:videoUrlCategory($category)}">{$category.term_name|videoSubstr=0,12}</a>
    </div>
  {/volist}
  </div>
</div>
{/block}
<!-- -->
{block name="footer"}{include file="widget/footer" /}{/block}