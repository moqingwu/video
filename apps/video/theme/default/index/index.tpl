{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords}" />
<meta name="description" content="{$seoDescription}" />
{if config('video.meta_referer')}<meta name="referrer" content="never">{/if}
{/block}
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
  <div class="row row-1 row-cols-2 row-cols-md-5">
    {volist name=":videoSelect(['status'=>'normal','limit'=>10,'sort'=>'info_update_time','order'=>'desc'])" id="video"}
    <div class="col px-2 mb-3">
      <div class="card shadow">
        <a href="{:videoUrlDetail($video)}">
          <img class="card-img-top cover5_{$video.info_type}" src="{:videoUrlImage($video['video_cover'])}" alt="{$video.info_name|DcHtml}">
        </a>
        <div class="card-body pt-2 pb-0 px-2">
          <a class="d-block info_name mb-2" href="{:videoUrlDetail($video)}" title="{$video.info_name|DcHtml}">{$video.info_name|DcSubstr=0,26}</a>
          <p class="small d-flex flex-row justify-content-between align-items-center">
            <font class="text-muted"><i class="text-purple fa fa-heart-o mr-1"></i>{$video.info_views}</font>
            <font class="text-muted">{$video.info_update_time|substr=0,10}</font>
          </p>
         </div>
      </div>
    </div>
  {/volist}
  </div>
  {include file="widget/ads960" /}
  {volist name=":videoCategorySelect(['status'=>'normal','limit'=>6,'sort'=>'term_order','order'=>'desc'])" id="category"}
  <div class="row row-1">
    <div class="col-12 px-2 mb-3">
      <div class="border-bottom pb-2 d-flex flex-row justify-content-between align-items-center">
        <a class="text-purple nav-title" href="{:videoUrlCategory($category)}">{$category.term_name}</a>
        <a class="text-purple" href="{:videoUrlCategory($category)}">更多<i class="fa fa fa-angle-double-right"></i></a>
      </div>
    </div>
  </div>
  <div class="row row-1 row-cols-2 row-cols-md-6 mb-3">
   {volist name=":videoSelect(['term_id'=>['eq',$category['term_id']],'status'=>'normal','limit'=>12,'sort'=>'info_id','order'=>'desc'])" id="video"}
    <div class="col px-2 mb-3">
      <a href="{:videoUrlDetail($video)}">
        <img class="w-100 rounded mb-2 img-fluid cover6_{$video.info_type}" src="{:videoUrlImage($video['video_cover'])}" alt="{$video.info_name|DcHtml}">
      </a>
      <a class="info_name" href="{:videoUrlDetail($video)}">{$video.info_name|videoSubstr=0,22}</a>
    </div>
  {/volist}
  </div>
  {/volist}
{include file="widget/friend" /}
</div>
{/block}
<!-- -->
{block name="footer"}{include file="widget/footer" /}{/block}