{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords}" />
<meta name="description" content="{$seoDescription}" />
{if config('video.meta_referer')}<meta name="referrer" content="never">{/if}
{/block}
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
  <div class="jumbotron jumbotron-fluid p-0 mb-0 pb-1">{$video_player}</div>
  <div class="bg-white p-3 mb-3">
    <div class="row">
        <div class="col-md-9 pt-2 mb-2 mb-md-0 text-truncate">
          <font class="font-weight h6">{$info_name}</font>
          {foreach name="category" item="item"}
          <a class="small text-purple ml-1" href="{:videoUrlCategory($item)}">{$item.term_name}</a>
          {/foreach}
          {foreach name="tag" item="item"}
          <a class="small text-purple ml-1" href="{:videoUrlTag($item)}">{$item.term_name}</a>
          {/foreach}
        </div>
        <div class="col-md-3">
          <div class="w-100 d-flex flex-row justify-content-between align-items-center">
          {volist name=":videoPrev($info_id)" id="prev"}
          <a class="btn btn-small btn-purple py-1" href="{:videoUrlDetail($prev)}" title="{$prev.info_name}">上一个</a>
          {/volist}
          {empty name="prev"}<a class="btn btn-small btn-purple py-1 disabled" href="javascript:;">上一个</a>{/empty}
          <a class="btn btn-small btn-purple py-1" href="{:DcUrl('video/value/up',['id'=>$info_id])}" data-toggle="infoUp">
          喜欢(<small class="h6 infoUpValue">{$video_up|number_format}</small>)
          </a>
          {volist name=":videoNext($info_id)" id="next"}
          <a class="btn btn-small btn-purple py-1" href="{:videoUrlDetail($next)}" title="{$next.info_name}">下一个</a>
          {/volist}
          {empty name="next"}<a class="btn btn-small btn-purple py-1 disabled" href="javascript:;">下一个</a>{/empty}
          </div>
        </div>
    </div>
  </div>
  {include file="widget/ads960" /}
  {if $info_content}
  <div class="card rounded-0 mb-3">
    <div class="card-body info_content">{$info_content|DcHtml}</div> 
  </div>
  {/if}
  <div class="row row-1">
    <div class="col-12 px-2 mb-3">
      <div class="border-bottom pb-2 d-flex flex-row justify-content-between align-items-center">
        <a class="text-purple nav-title" href="{:DcUrl('video/category/index',['slug'=>'views'])}">大家都在看</a>
        <a class="text-purple" href="{:DcUrl('video/category/index',['slug'=>'views'])}">更多<i class="fa fa fa-angle-double-right"></i></a>
      </div>
    </div>
  </div>
  <div class="row row-1 row-cols-2 row-cols-md-6 mb-3">
   {volist name=":videoSelect(['term_id'=>['in',$category_id],'status'=>'normal','limit'=>12,'sort'=>'info_create_time','order'=>'desc'])" id="video"}
    <div class="col px-2 mb-3">
      <a href="{:videoUrlDetail($video)}">
        <img class="w-100 rounded mb-2 img-fluid cover6_{$video.info_type}" src="{:videoUrlImage($video['video_cover'])}" alt="{$video.info_name|DcHtml}">
      </a>
      <a class="info_name" href="{:videoUrlDetail($video)}">{$video.info_name|videoSubstr=0,24}</a>
    </div>
  {/volist}
  </div>
</div>
{/block}
<!-- -->
{block name="footer"}{include file="widget/footer" /}{/block}