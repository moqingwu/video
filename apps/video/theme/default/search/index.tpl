{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle}－{:config('common.site_name')}</title>
<meta name="keywords" content="{$seoKeywords}" />
<meta name="description" content="{$seoDescription}" />
{if config('video.meta_referer')}<meta name="referrer" content="never">{/if}
{/block}
{block name="header"}{include file="widget/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container">
  <ol class="breadcrumb bg-white mb-3">
    <li class="breadcrumb-item"><a href="{:DcUrl('video/index/index')}">首页</a></li>
    <li class="breadcrumb-item active">{:config('common.site_name')}为您找到相关结果约{$total|number_format}个</li>
  </ol>
  {volist name="data" id="video"}
  <div class="media rounded bg-white p-3 mb-3">
    <a class="text-light d-none d-md-inline" href="{:videoUrlDetail($video)}">
      <img class="mr-3 rounded" src="{:videoUrlImage($video['video_cover'])}" alt="{$dc.vod_title}" width="160" height="90">
    </a>
    <div class="media-body pt-2">
      <h6 class="mb-3"><a href="{:videoUrlDetail($video)}">{$video.info_name}</a></h6>
      <h6 class="mb-3 small">{$video.category_name|implode=' / '} / {$video.tag_name|implode=' / '}</h6>
      <p class="small text-muted pb-0 mb-0">更新时间：{$video.info_update_time|substr=0,10} 观看人数：{$video.info_views|number_format}</p>
    </div> 
  </div>
  {/volist}
  <!---->
  {gt name="last_page" value="1"}
  <div class="border bg-white pagesmall py-2 mb-2 d-md-none">{:DcPageSimple($current_page, $last_page, $pagePath)}</div>
  <div class="border bg-white page py-2 mb-2 d-none d-md-block">{:DcPage($page, $per_page, $total, $pagePath)}</div>
  {/gt}
</div>
{/block}
<!-- -->
{block name="footer"}{include file="widget/footer" /}{/block}