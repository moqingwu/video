<div class="container">
  <div class="row my-4">
    <div class="col col-md-7 d-none d-md-inline">
      <a class="navbar-brand p-0" href="{$path_root}">{:config('common.site_name')}</a>
    </div>
    <form class="col col-md-5" id="search" action="{:videoUrlSearch('video/search/index')}" method="post">
      <div class="input-group">
        <input class="form-control" type="text" name="searchText" id="searchText" value="{$searchText}" placeholder="搜索..." autocomplete="off" required>
        {if config('video.search_list')}
        <div class="input-group-prepend position-relative" id="searchDropdown">
          <button class="btn btn-purple dropdown-toggle" type="button" data-toggle="dropdown">视频</button>
          <div class="dropdown-menu">
            {volist name=":videoSearchList()" id="searchName" offset="0" length="10"}
            <a class="dropdown-item" href="javascrit:;" data-href="{:videoUrlSearch($searchName)}">{:lang($searchName)}</a>
            {/volist}
          </div>
        </div>
        {/if}
        <div class="input-group-append">
          <button class="btn btn-purple" type="submit"><i class="fa fa-search"></i></button>
        </div>
      </div>
    </form>
  </div>
</div>
<nav class="navbar navbar-expand-md navbar-dark bg-purple mb-3">
<div class="container">
  <a class="navbar-brand d-md-none" href="{$path_root}">{:config('common.site_name')}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav">
    <span class="navbar-toggler-icon"></span>
  </button>
  {if $user['user_id']}
    {assign name="navbars" value=":videoNavsSelect(['module'=>['in','common,user,video'],'controll'=>['in','category,navs'],'type'=>'navbar','status'=>['in','normal,private']])" /}
  {else/}
    {assign name="navbars" value=":videoNavsSelect(['module'=>['in','common,user,video'],'controll'=>['in','category,navs'],'type'=>'navbar','status'=>['in','normal,public']])" /}
  {/if}
  <div class="collapse navbar-collapse" id="nav">
    <hr class="w-100 mb-0 d-md-none">
    <ul class="navbar-nav w-100 d-flex flex-row flex-wrap">
    {volist name="navbars" id="navbar" offset="0" length="30"}
      {if $navbar['_child']}
        <li class="position-relative dropdown nav-item nav-mx {:videoNavActive($module.$controll.$action, $term_id, $navbar['navs_active'], $navbar['navs_id'])}" id="term-{$navbar.navs_id}">
          <a class="nav-link dropdown-toggle" href="javascript:;" data-toggle="dropdown">{$navbar.navs_name|DcSubstr=0,6,false}</a>
          <div class="dropdown-menu">
            {volist name="navbar._child" id="navSon"}
            <a class="dropdown-item {:videoNavActive($module.$controll.$action, $term_id, $navSon['navs_active'], $navSon['navs_id'])}" href="{$navSon.navs_link}" target="{$navSon.navs_target}">{$navSon.navs_name|DcSubstr=0,6,false}</a>
            {/volist}
          </div>
        </li>
      {else/}
        <li class="nav-item nav-mx {:videoNavActive($module.$controll.$action, $term_id, $navbar['navs_active'], $navbar['navs_id'])}" id="term-{$navbar.navs_id}">
          <a class="nav-link" href="{$navbar.navs_link}" target="{$navbar.navs_target}">{$navbar.navs_name|DcSubstr=0,6,false}</a>
        </li>
      {/if}
    {/volist}
    </ul>
  </div>
</div>
</nav>