<div class="container small">
  <p class="text-md-center">
    {volist name=":DcTermNavbar(['status'=>'normal','controll'=>'navs','type'=>'link','sort'=>'term_order','order'=>'desc'])" id="link"}
    <a class="text-muted" href="{$link.navs_link}">{$link.navs_name|DcSubstr=0,6,false}</a>
    {/volist}
  </p>
  <p class="text-center">
    Copyright © 2020-2022 {:config('common.site_domain')} All rights reserved.
  </p>
  <p class="text-center">
    {if config('common.site_icp')}<a class="mr-1" href="https://beian.miit.gov.cn" target="_blank">{:config('common.site_icp')}</a>{/if}
    {if config('common.site_email')}联系我们：{:config('common.site_email')}{/if}
  </p>
</div>