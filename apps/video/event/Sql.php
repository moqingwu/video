<?php
namespace app\video\event;

class Sql
{
    /**
    * 安装时触发
    * @return bool 只有返回true时才会往下执行
    */
	public function install()
    {
        //初始配置
        model('video/Datas','loglic')->insertConfig();
        
        //初始字段
        model('video/Datas','loglic')->insertField();
        
        //初始路由
        model('video/Datas','loglic')->insertRoute();
        
        //后台菜单
        model('video/Datas','loglic')->insertMenu();
        
        //分类/标签/导航
        model('video/Datas','loglic')->insertTerm();
        
        //采集规则
        model('video/Datas','loglic')->insertCollect();
        
        //测试数据
        model('video/Datas','loglic')->insertDetail();
        
        //清空缓存
        \think\Cache::clear();
        
        //返回结果
        return true;
	}
    
    /**
    * 升级时触发
    * @return bool 只有返回true时才会往下执行
    */
    public function upgrade()
    {
        //更新扩展字段
        model('video/Datas','loglic')->updateMenu();
        
        //更新基础信息
        model('video/Datas','loglic')->updateStatus();

        //更新打包配置
        model('video/Datas','loglic')->updatePack();
        
        //清空缓存
        \think\Cache::clear();

        return true;
    }
    
    /**
    * 卸载时触发
    * @return bool 只有返回true时才会往下执行
    */
    public function remove()
    {
        return model('video/Datas','loglic')->remove();
    }
    
    /**
    * 删除时触发
    * @return bool 只有返回true时才会往下执行
    */
    public function unInstall()
    {
        return model('video/Datas','loglic')->delete();
	}
}