<?php
namespace app\video\event;

use app\common\controller\Addon;

class Baidu extends Addon
{
    public function _initialize()
    {
        parent::_initialize();
    }
    
    public function index()
    {
        $this->assign('fields', model('video/Baidu','loglic')->fields($this->query));
        
        $this->assign('fieldsForm', model('video/Baidu','loglic')->fieldsForm($this->forumUrls()));
        
        return $this->fetch('video@baidu/index');
	}
    
    public function push()
    {
        $this->checkApi();
        //参数
        $args = array();
        $args['cache']    = false;
        $args['field']    = 'info_id,info_parent,info_name,info_slug,info_action';
        $args['with']     = 'term,term_map';
        $args['limit']    = DcEmpty(input('request.pageSize'), 10);
        $args['page']     = DcEmpty(input('request.pageNumber'), 1);
        $args['sort']     = 'info_id';
        $args['order']    = 'desc';
        $args['status']   = 'normal';
        //查询数据
        $list = videoSelect($args);
        if( is_null($list) ){
            $this->error(lang('empty'));
        }
        //拼装网址
        $urls = [];
        foreach($list['data'] as $key=>$value){
            $url = $this->frontUrl($this->request->root(true).videoUrlDetail($value));
            array_push($urls,$url);
        }
        //推送数据
        $result = json_decode($this->post($urls),true);
        if($result['error'] || !$result){
            $this->error(DcEmpty($result['message'],'百度API地址不正确'),'addon/index/index?module=video&controll=baidu&action=index',false,5);
        }
        //记录断点
        DcCache('baiduPushVideo', $args['page']+1, 0);
        //跳转下一页
        if($args['page'] < $list['last_page']){
            $this->success('第'.$args['page'].'页推送完成','addon/index/index?module=video&controll=baidu&action=push&pageSize='.$args['limit'].'&pageNumber='.($args['page']+1), false, 2);
        }else{
            DcCache('baiduPushVideo', null);
            $this->success('推送完成','addon/index/index?module=video&controll=baidu&action=index');
        }
	}
    
    //表单提交
    public function forum()
    {
        $this->checkApi();
        //获取表单
        $urls = explode(" ",videoTrim(input('post.urls')));
        if(!$urls){
            $this->error(lang('empty'));
        }
        //推送数据
        $result = json_decode($this->post($urls),true);
        if($result['error'] || !$result){
            $this->error(DcEmpty($result['message'],'百度API地址不正确'));
        }
        $this->success('推送完成');
    }
    
    //推送接口
    private function post($urls=[])
    {
        $ch = curl_init();
        $options =  array(
            CURLOPT_URL => config('video.baidu_push'),
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => implode("\n", $urls),
            CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
        );
        curl_setopt_array($ch, $options);
        return curl_exec($ch);
    }
    
    //验证apiurl
    private function checkApi()
    {
        if(!config('video.baidu_push')){
            $this->error('请先填写百度API提交地址','addon/index/index?module=video&controll=config&action=index');
        }
    }
    
    //生成前台链接
    private function frontUrl($url='')
    {
        return str_replace('/'.basename($this->request->baseFile()), '', $url);
    }
    
    //默认网页
    private function forumUrls()
    {
        $urls = [];
        $urls[0] = $this->frontUrl($this->request->root(true));
        $urls[1] = $this->frontUrl($this->request->root(true).DcUrl('video/category/index',['slug'=>'types']));
        $urls[2] = $this->frontUrl($this->request->root(true).DcUrl('video/category/index',['slug'=>'tags']));
        $urls[3] = $this->frontUrl($this->request->root(true).DcUrl('video/category/index',['slug'=>'views']));
        return $urls;
    }
}