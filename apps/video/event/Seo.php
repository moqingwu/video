<?php
namespace app\video\event;

use app\common\controller\Addon;

class Seo extends Addon
{
    public function _initialize()
    {
        parent::_initialize();
    }
    
    public function index()
    {
        $fields = model('video/Seo','loglic')->fields($this->query);
        
        $this->assign('fields', DcFormItems($fields));
        
        return $this->fetch('video@seo/index');
    }
    
    public function update()
    {
        $post = input('post.');
        //更新配置
        $status = \daicuo\Op::write($post, 'video', 'config', 'system', 0, 'yes');
		if( !$status ){
		    $this->error(lang('fail'));
        }
        //伪静态路由
        $this->rewriteRoute($post);
        //返回结果
        $this->success(lang('success'));
    }
    
    //配置伪静态
    private function rewriteRoute($post=[])
    {
        //批量删除路由
        \daicuo\Op::delete_all([
            'op_name'   => ['eq','site_route'],
            'op_module' => ['eq','video'],
        ]);
        //批量添加路由
        $result = \daicuo\Route::save_all([
            [
                'rule'        => $post['rewrite_index'],
                'address'     => 'video/index/index',
                'method'      => '*',
                'op_module'   => 'video',
                'op_controll' => 'index',
                'op_action'   => 'index',
            ],
            [
                'rule'        => $post['rewrite_category'],
                'address'     => 'video/category/index',
                'method'      => '*',
                'op_module'   => 'video',
                'op_controll' => 'category',
                'op_action'   => 'index',
            ],
            [
                'rule'        => $post['rewrite_tag'],
                'address'     => 'video/tag/index',
                'method'      => '*',
                'op_module'   => 'video',
                'op_controll' => 'tag',
                'op_action'   => 'index',
            ],
            [
                'rule'        => $post['rewrite_search'],
                'address'     => 'video/search/index',
                'method'      => '*',
                'op_module'   => 'video',
                'op_controll' => 'search',
                'op_action'   => 'index',
            ],
            [
                'rule'        => $post['rewrite_detail'],
                'address'     => 'video/detail/index',
                'method'      => '*',
                'op_module'   => 'video',
                'op_controll' => 'detail',
                'op_action'   => 'index',
            ],
        ]);
        //清理全局缓存
        DcCache('route_all', null);
    }
}