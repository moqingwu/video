<?php
namespace app\video\event;

use app\common\controller\Addon;

class Admin extends Addon
{
    public function _initialize()
    {
        parent::_initialize();
    }
    
    //定义表单字段列表
    protected function fields($data=[])
    {
        return model('video/Detail','loglic')->fields($data);
    }
    
    //定义表单初始数据
    protected function formData()
    {
        if( $id = input('id/d',0) ){
            return model('common/Info','loglic')->getId($id, false);
		}
        return [];
    }
    
    //定义表格数据（JSON）
    protected function ajaxJson()
    {
        $args = array();
        $args['cache']    = false;
        $args['search']   = $this->query['searchText'];
        $args['limit']    = DcEmpty($this->query['pageSize'], 30);
        $args['page']     = DcEmpty($this->query['pageNumber'], 1);
        $args['sort']     = DcEmpty($this->query['sortName'], 'info_id');
        $args['order']    = DcEmpty($this->query['sortOrder'], 'desc');
        $args['module']   = $this->query['info_module'];
        $args['controll'] = $this->query['info_controll'];
        $args['action']   = $this->query['info_action'];
        $args['type']     = $this->query['info_type'];
        $args['status']   = $this->query['info_status'];
        $args['term_id']  = $this->query['category_id'];
        //按META字段条件筛选
        $args['meta_query'] = videoMetaQuery($this->query);
        //按META字段排序
        if( !in_array($args['sort'],['info_id','info_order','info_views','info_hits','info_create_time','info_update_time']) ){
            $args['meta_key'] = $args['sort'];
            $args['sort']     = 'meta_value_num';
        }
        //查询数据
        $list = videoSelect( DcArrayEmpty($args) );
        if( is_null($list) ){
            return [];
        }
        //拼装数据
        foreach($list['data'] as $key=>$value){
            $list['data'][$key]['video_category'] = implode(',',$value['category_name']);
        }
        return $list;
    }
    
    public function preview()
    {
        if( !$id = input('id/d',0) ){
            $this->error(lang('mustIn'));
        }
        //去掉后台入口文件
        $url = str_replace($this->request->baseFile(), '', videoUrlDetail( videoGetId($id,false)) );
        //跳转至前台
        $this->redirect($url,302);
    }
    
    public function save()
    {

        if( !videoSave(input('post.'), true) ){
            $this->error(\daicuo\Info::getError());
        }

        $this->success(lang('success'));
    }
    
    public function delete()
    {
        videoDelete(input('id/a'));
        
        $this->success(lang('success'));
    }
    
    public function update()
    {
        if( !videoUpdate(input('post.'), true) ){
            $this->error(\daicuo\Info::getError());
        }
        $this->success(lang('success'));
    }
    
    public function status()
    {
        if( !$ids = input('post.id/a') ){
            $this->error(lang('errorIds'));
        }
        $data = [];
        $data['info_status'] = input('request.value/s', 'hidden');
        dbUpdate('common/Info',['info_id'=>['in',$ids]], $data);
        $this->success(lang('success'));
    }
}