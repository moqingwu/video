<?php
namespace app\video\event;

use app\common\controller\Addon;

class Config extends Addon
{
	public function _initialize()
    {
		parent::_initialize();
	}

	public function index()
    {
        $fields = model('video/Config','loglic')->fields($this->query);
        
        $this->assign('fields', DcFormItems($fields));
        
        return $this->fetch('video@config/index');
	}
    
    public function update()
    {
        $status = \daicuo\Op::write(input('post.'), 'video', 'config', 'system', 0, 'yes');
		if( !$status ){
		    $this->error(lang('fail'));
        }
        $this->success(lang('success'));
	}
}