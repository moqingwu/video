<?php
namespace app\video\validate;

use think\Validate;

class Detail extends Validate
{
	protected $rule = [
        'info_module'   => 'require',
        'info_name'     => 'require',
        'info_id'       => 'require',
        'video_referer' => 'unique_referer',
	];
	
	protected $message = [
        'info_module.require' => '{%info_module_require}',
		'info_name.require'   => '{%info_name_require}',      
        'info_id.require'     => '{%video_id_require}',
        'info_slug.unique'    => '{%video_slug_unique}',
	];
	
	protected $scene = [
        //新增
		'save'          => ['info_module','info_name'],
        //修改
		'update'        => ['info_module','info_name','info_id'],
        //别名唯一
        'slugUnique'    => ['info_module','info_name','info_slug'=>'require|unique_slug'],
        //来源唯一
        'refererUnique' => ['info_module','info_name','video_referer'],
	];
    
    //别名URL唯一
    protected function unique_slug($value, $rule, $data, $field)
    {
        $where = array();
        $where['info_module'] = ['eq','video'];
        $where['info_slug']   = ['eq',$value];
        $info = db('info')->where($where)->value('info_id');
        //无记录直接通过
        if(is_null($info)){
            return true;
        }
        //已有记录
		return lang('video_info_slug_unique');
	}
    
    //来源网址唯一
    protected function unique_referer($value, $rule, $data, $field)
    {
        $where = array();
        $where['info_meta_key']   = ['eq','video_referer'];
        $where['info_meta_value'] = ['eq',$value];
        $info = db('infoMeta')->where($where)->value('info_meta_id');
        //无记录直接通过
        if(is_null($info)){
            return true;
        }
        //已有记录
		return lang('video_referer_unique');
	}
}