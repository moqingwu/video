<?php
namespace app\video\controller;

use app\video\controller\Common;

class Detail extends Common
{
    public function _initialize()
    {
        parent::_initialize();
    }
    
    public function index()
    {
        if( isset($this->query['id']) ){
            $info = videoGetId($this->query['id']);
        }elseif( isset($this->query['slug']) ){
            $info = videoGetSlug($this->query['slug']);
        }elseif( isset($this->query['name']) ){
            $info = videoGetName($this->query['name']);
        }else{
            $this->error(lang('video_error_params'),'video/index/index');
        }
        //数据判断
        if(!$info){
            $this->error(lang('video_error_empty'),'video/index/index');
        }
        //SEO标签
        $info['seoTitle']       = videoSeo(DcEmpty($info['info_title'],$info['info_name']));
        $info['seoKeywords']    = videoSeo(DcEmpty($info['info_keywords'],$info['info_name']));
        $info['seoDescription'] = videoSeo(DcEmpty($info['info_description'],videoSubstr($info['info_excerpt'],0,100)));
        //分享链接
        if(DcBool(config('common.app_domain'))){
            $info['video_share'] = videoUrlDetail($info);
        }else{
            $info['video_share'] = $this->site['domain'].videoUrlDetail($info);
        }
        //播放器
        $info['video_player'] = DcPlayer(['poster'=>DcUrlAttachment($info['video_cover']),'url'=>DcUrlAttachment($info['info_excerpt'])]);
        //主分类ID
        $info['term_id']      = intval($info['category_id'][0]);
        //增加人气值
        videoInfoInc($info['info_id'],'info_views');
        //变量赋值
        $this->assign($info);
        //自定义模板
        if($info['video_tpl']){
            return $this->fetch($info['video_tpl']);
        }
        //按类型加载
        return $this->fetch(DcEmpty($info['info_type'],'index'));
    }
}