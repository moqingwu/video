<?php
namespace app\video\controller;

use app\common\controller\Front;

class Common extends Front
{
    //初始化
    public function _initialize()
    {
        //请求验证
        if( !$this->requestCheck() ){
            $this->error(lang('video/error/rest'), 'video/index/index');
        }
        //继承上级
        parent::_initialize();
    }
    
    /**
     * 验证请求是否合法（防CC、假墙功能）
     * @param string $ip 必需;客户端IP;默认：127.0.0.1
     * @param string $agent 必需;浏览器头;默认：default
     * @return bool true|false
     */
    private function requestCheck()
    {
        //后台验证开关
        $configMax = intval(config('video.request_max'));
        if($configMax < 1){
            return true;
        }
        //客户端唯一标识（IP+浏览器头）
        $client = md5($this->request->ip().$this->request->header('user-agent'));
        //60秒内最大请求次数
        $requestMax = intval(DcCache('request'.$client));
        if($requestMax > $configMax){
            return false;
        }
        DcCache('request'.$client, $requestMax+1, 60);
        //未超出限制
        return true;
    }
}