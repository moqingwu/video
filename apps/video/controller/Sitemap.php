<?php
namespace app\video\controller;

use app\video\controller\Common;

class Sitemap extends Common
{

    public function _initialize()
    {
        parent::_initialize();
    }
    
    public function index()
    {
        $args = [];
        $args['cache']    = true;
        $args['status']   = 'normal';
        $args['field']    = 'info_id,info_slug,info_name,info_status,info_update_time';
        $args['with']     = 'term';
        $args['limit']    = videoPageSize(config('video.limit_sitemap'));
        $args['page']     = DcEmpty($this->query['pageNumber'], $this->site['page']);
        $args['sort']     = DcEmpty($this->query['sortName'], 'info_update_time');
        $args['order']    = DcEmpty($this->query['sortOrder'], 'desc');
        //数据查询
        $list = videoSelect($args);
        //域名前缀
        config('common.app_domain',true);
        //拼装结果
        $result = [];
        foreach($list['data'] as $key=>$value){
            array_push($result,videoUrlDetail([
                'info_id'       => $value['info_id'],
                'info_slug'     => $value['info_slug'],
                'info_name'     => $value['info_name'],
                'category_id'   => $value['category_id'],
                'category_slug' => $value['category_slug'],
                'category_name' => $value['category_name'],
            ]));
        }
        unset($list);
        return implode("\n",$result);
    }
}