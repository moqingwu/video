<?php
namespace app\video\controller;

use app\common\controller\Api;

class Data extends Api
{
    protected $auth = [
         'check'       => true,
         'none_login'  => ['video/data/login'],
         'none_right'  => ['video/data/category','video/data/detail'],
         'error_login' => 'video/index/index',
         'error_right' => 'video/index/index',
    ];
    
    public function _initialize()
    {
		parent::_initialize();
    }
    
    public function login()
    {
        $this->error(lang('empty'));
    }
    
    //分类接口
    public function category()
    {
        $item = videoCategorySelect([
            'cache'    => true,
            'status'   => 'normal',
            'result'   => 'array',
            'limit'    => 0,
            'page'     => 0,
            'sort'     => 'term_id',
            'order'    => 'asc',
            'with'     => '',
            'action'   => 'index',
            'field'    => 'term_id,term_name,term_slug',
        ]);
        foreach($item as $key=>$value){
            unset($item[$key]['term_status_text']);
        }
        $this->success(lang('success'),$item);
    }
    
    //内容列表接口
    //detail?termId=1&pageNumber=1
    public function detail()
    {
        //初始参数
        $args = [
            'cache'      => true,
            'status'     => 'normal',
            'sort'       => 'info_update_time',
            'order'      => videoSortOrder(input('request.sortOrder/s','asc')),
            'limit'      => 10,
            'page'       => input('request.pageNumber/d',1),
            'with'       => '',//info_meta,term,term_map
            'field'      => 'info_id,info_name,info_update_time,info_type',
        ];
        //分类筛选参数
        if($this->query['termId']){
            $args['term_id'] = intval($this->query['termId']);
            $args['field']   = 'info.info_id,info_name,info_update_time,info_type';
        }
        //时间限制参数
        if($this->query['time']){
            $args['where']['info_update_time'] = ['> time',$this->query['time']];
        }
        //数据查询
        if(!$item = videoSelect($args)){
            $this->error(lang('empty'));
        }
        //重新组合数据
        $json = [];
        $json['total']        = $item['total'];
        $json['per_page']     = $item['per_page'];
        $json['current_page'] = $item['current_page'];
        $json['last_page']    = $item['last_page'];
        $json['list']         = [];
        //拼装数据
        foreach($item['data'] as $key=>$value){
            unset($value['info_status_text']);
            $json['list'][$key] = $value;
            $json['list'][$key]['video_referer'] = md5($this->site['domain'].'/video/'.$value['info_id']);
        }
        unset($item);
        //返回数据
        $this->success(lang('success'),$json);
    }
    
    //单个内容详情接口
    //index?id=1
    public function index()
    {
        $id = input('request.id/f',1);
        $data = videoGet([
            'cache'      => true,
            'status'     => 'normal',
            'module'     => 'video',
            'id'         => ['eq',$id],
            'with'       => 'info_meta,term',
            'field'      => 'info_id,info_name,info_slug,info_excerpt,info_content,info_create_time,info_update_time,info_order,info_type,info_views,info_hits',
        ]);
        if(!$data){
            $this->error(lang('empty'));
        }
        //禁用远程图片接口
        config('common.upload_referer',false);
        //格式化数据
        $this->success(lang('success'),$this->detailValue($data));
    }
    
    //内容详情格式化
    private function detailValue($value=[])
    {
        //图标
        if($value['video_cover']){
            $value['video_cover']   = videoUrlImage($value['video_cover']);
        }
        //头条
        if($value['video_slide']){
            $value['video_slide']   = videoUrlImage($value['video_slide']);
        }
        //来源
        $value['video_referer']     = md5($this->site['domain'].'/video/'.$value['info_id']);
        //分享链接
        if(DcBool(config('common.app_domain'))){
            $value['video_share'] = videoUrlDetail($value);
        }else{
            $value['video_share'] = $this->site['domain'].videoUrlDetail($value);
        }
        //字段过滤
        unset($value['info_status_text']);
        unset($value['category']);
        unset($value['category_slug']);
        unset($value['tag']);
        unset($value['tag_slug']);
        unset($value['video_tpl']);
        //返回
        return $value;
    }
}