<?php
namespace app\video\controller;

use app\common\controller\Front;

class Post extends Front
{
    public function _initialize()
    {
		parent::_initialize();
    }
    
    public function index()
    {
        //必需设置密码
        if(!config('video.post_pwd')){
            return json(['code'=>0,'msg'=>lang('video/post/config')]);
        }
        //获取表单数据
        $post = input('post.');
        //入库密码字段
        if(!$post['post_pwd']){
            return json(['code'=>0,'msg'=>lang('video/post/empty')]);
        }
        //密码验证
        if($post['post_pwd'] != config('video.post_pwd')){
            return json(['code'=>0,'msg'=>lang('video/post/wrong')]);
        }
        //检查是否已存在来源
        if($this->refererUnique($post['video_referer'])){
            return json(['code'=>0,'msg'=>lang('video/post/referer')]);
        }
        //保存数据
        if( !$id = videoSave($post, true) ){
            return json(['code'=>0,'msg'=>lang('video/post/fail')]);
        }
        //添加成功
        return json(['code'=>1,'data'=>$id]);
    }
    
    //来源网址验证唯一（返回id|null）
    private function refererUnique($referer='')
    {
        if(!$referer){
            return null;
        }
        $where = array();
        $where['info_meta_key']   = ['eq','video_referer'];
        $where['info_meta_value'] = ['eq',$referer];
        return db('infoMeta')->where($where)->value('info_meta_id');
    }
    
    //入库字段
    public function test()
    {
        $data = [];
        $data['post_pwd']      = '123456';//入库密码字段（必填、需与后台设置的一致）
        $data['category_name'] = '人文历史,情感心理';//分类字段，多个用逗号分隔
        $data['tag_name']      = 'qq,mp4';//标签字段，多个用逗号分隔
        $data['info_name']     = 'this is post test';//名称
        $data['info_excerpt']  = 'https://v.qq.com/x/cover/mzc00200syg68ky.html';//播放地址
        $data['info_content']  = 'this is content';//详情
        $data['info_type']     = 'index';//形式(index|vertical)
        $data['video_cover']   = 'https://cdn.daicuo.cc/images/daicuo/favicon.ico';//封面字段
        $data['video_referer'] = 'https://v.qq.com/x/cover/mzc00200syg68ky.html';//不提交此字段则不验证是否已采集
        return json($data);
    }
}