<?php
namespace app\video\controller;

use app\video\controller\Front;

class Search extends Common
{
    
    public function _initialize()
    {
        //请求过滤
        $this->request->filter('trim,strip_tags,htmlspecialchars');
        //post请求
        if( $this->request->isPost() ){
            $this->redirect(videoUrlSearch('video/search/index',['searchText'=>input('post.searchText/s')]),302);
        }
        //继承上级
        parent::_initialize();
    }
    
    //站内搜索
    public function index()
    {
        //是否有关键词
        if( strlen($this->query['searchText']) < 2 ){
            $this->error(lang('video/error/keyword'),'video/index/index');
        }
        
        //地址栏
        $info = [];
        $info['searchText'] = $this->query['searchText'];
        $info['pageSize']   = videoPageSize(config('video.limit_search'));
        $info['pageNumber'] = $this->site['page'];
        $info['sortName']   = 'info_order desc,info_views';
        $info['sortOrder']  = 'desc';
        
        //分页路径
        $info['pagePath'] = videoUrlSearch('video/search/index',[
            'searchText' => $info['searchText'],
            'pageNumber' => '[PAGE]',
        ]);
        
        //SEO标签
        $info['seoTitle']       = videoSeo(str_replace('[searchText]',$info['searchText'],config('video.search_title')),$this->site['page']);
        $info['seoKeywords']    = videoSeo(str_replace('[searchText]',$info['searchText'],config('video.search_keywords')),$this->site['page']);
        $info['seoDescription'] = videoSeo(str_replace('[searchText]',$info['searchText'],config('video.search_description')),$this->site['page']);
        
        //查询数据
        $item = videoSelect([
            'cache'   => true,
            'status'  =>'normal',
            'limit'   => $info['pageSize'],
            'page'    => $info['pageNumber'],
            'sort'    => $info['sortName'],
            'order'   => $info['sortOrder'],
            //'search'  => $info['searchText'],
            'name'    => ['like','%'.$info['searchText'].'%'],
        ]);
        if($item){
            $this->assign($item);
        }
        
        //变量赋值
        $this->assign($info);
        
        //加载模板
        return $this->fetch();
    }
}