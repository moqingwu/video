<?php
namespace app\video\controller;

use app\video\controller\Common;

class Index extends Common
{
    public function _initialize()
    {
        parent::_initialize();
    }
    
    public function index()
    {
        $info = [];
        //分页路径
        $info['pagePath']       = DcUrl('video/index/index',['pageNumber'=>'[PAGE]']);
        //分页参数
        $info['pageSize']       = intval(config('video.limit_index'));
        $info['pageNumber']     = $this->site['page'];
        //SEO标签
        $info['seoTitle']       = videoSeo(config('video.index_title'),$this->site['page']);
        $info['seoKeywords']    = videoSeo(config('video.index_keywords'),$this->site['page']);
        $info['seoDescription'] = videoSeo(config('video.index_description'),$this->site['page']);
        //是否分页查询
        if($info['pageSize']){
            $item = videoSelect([
                'cache'    => true,
                'status'   => 'normal',
                'limit'    => $info['pageSize'],
                'page'     => $info['pageNumber'],
                'sort'     => 'info_order desc,info_update_time',
                'order'    => 'desc',
            ]);
            $this->assign($item);
        }
        //变量赋值
        $this->assign($info);
        //加载模板
        return $this->fetch();
    }
}