<?php
namespace app\video\controller;

use app\video\controller\Common;

class Category extends Common
{

    public function _initialize()
    {
        parent::_initialize();
    }
    
    public function index()
    {
        if( isset($this->query['id']) ){
            $term = videoCategoryId($this->query['id']);
        }elseif( isset($this->query['slug']) ){
            $term = videoCategorySlug($this->query['slug']);
        }elseif( isset($this->query['name']) ){
            $term = videoCategoryName($this->query['name']);
        }else{
            $this->error(lang('video/error/params'),'video/index/index');
        }
        //数据为空
        if(!$term){
            $this->error(lang('video/error/empty'),'video/index/index');
        }
        //分页路径
        $term['pagePath']   = videoUrlCategory($term,'[PAGE]');
        //分页数量
        if(is_numeric($term['term_limit'])){
            $term['pageSize'] = $term['term_limit'];
        }else{
            $term['pageSize'] = intval(config('video.limit_category'));
        }
        //地址栏
        $term['pageNumber'] = $this->site['page'];
        $term['sortName']   = 'info_order desc,info_update_time';
        $term['sortOrder']  = 'desc';
        //SEO标签
        $term['seoTitle']       = videoSeo(DcEmpty($term['term_title'],$term['term_name']),$this->site['page']);
        $term['seoKeywords']    = videoSeo(DcEmpty($term['term_keywords'],$term['term_name']),$this->site['page']);
        $term['seoDescription'] = videoSeo(DcEmpty($term['term_description'],$term['term_name']),$this->site['page']);
        //分页数据
        if($term['pageSize']){
            $item = videoSelect([
                'cache'   => true,
                'status'  =>'normal',
                'term_id' => $term['term_id'],
                'limit'   => $term['pageSize'],
                'page'    => $term['pageNumber'],
                'sort'    => $term['sortName'],
                'order'   => $term['sortOrder'],
            ]);
            //变量赋值
            if($item){
                $this->assign($item);
            }
        }
        //变量赋值
        $this->assign($term);
        //加载模板
        if($term['term_tpl']){
            return $this->fetch($term['term_tpl']);
        }
        //默认加载
        return $this->fetch('index');
    }
    
    //空操作
    public function _empty()
    {
        $this->redirect('video/category/index', $this->query, 302);
    }
}