<?php
/**
 * 按条件获取多个分类
 * @version 1.0.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $sort 可选;排序字段名;默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $result 可选;模型名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed 查询结果obj|null
 */
function videoCategorySelect($args=[])
{
    return model('common/Term','loglic')->select( DcArrayArgs($args,[
        'cache'    => true,
        'result'   => 'array',
        'controll' => 'category',
        'module'   => 'video',
    ]) );
}

/**
 * 按ID获取分类信息
 * @version 1.0.0 首次引入
 * @param int $value 必需;Id值;默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed 查询结果(array|null)
 */
function videoCategoryId($value='', $cache=true, $status='normal')
{
    $args = [
        'module'   => 'video',
        'controll' => 'category',
        'cache'    => $cache,
        'status'   => $status,
        'id'       => $value,
    ];
    return model('common/Term','loglic')->get($args);
}

/**
 * 按别名获取分类信息
 * @version 1.0.0 首次引入
 * @param string $value 必需;别名值;默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed 查询结果(array|null)
 */
function videoCategorySlug($value='', $cache=true, $status='normal')
{
    $args = [
        'module'   => 'video',
        'controll' => 'category',
        'cache'    => $cache,
        'status'   => $status,
        'slug'     => $value,
    ];
    return model('common/Term','loglic')->get($args);
}

/**
 * 按名称获取分类信息
 * @version 1.0.0 首次引入
 * @param string $value 必需;分类名称;默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed 查询结果(array|null)
 */
function videoCategoryName($value='', $cache=true, $status='normal')
{
    $args = [
        'module'   => 'video',
        'controll' => 'category',
        'cache'    => $cache,
        'status'   => $status,
        'name'     => $value,
    ];
    return model('common/Term','loglic')->get($args);
}

/**
 * 通过分类名获取分类ID（每个应用）
 * @version 1.0.1 首次引入
 * @param mixed $name 必需;标签名;默认：空
 * @param bool $autoSave可选;是否自动新增;默认：false
 * @return array 查询结果
 */
function videoCategoryAuto($name=[], $autoSave=false){
    return model('common/Term','loglic')->nameToId($name, 'video', 'category', $autoSave);
}

/**
 * 按条件获取多个标签信息
 * @version 1.0.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $sort 可选;排序字段名;默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $result 可选;模型名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed 查询结果obj|null
 */
function videoTagSelect($args=[])
{
    return model('common/Term','loglic')->select( DcArrayArgs($args,[
        'cache'    => true,
        'result'   => 'array',
        'controll' => 'tag',
        'module'   => 'video',
    ]) );
}

/**
 * 按ID快速获取标签信息
 * @version 1.0.1 首次引入
 * @param int $value 必需;Id值;默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed 查询结果(array|null)
 */
function videoTagId($value='', $cache=true, $status='normal')
{
    $args = [
        'module'   => 'video',
        'controll' => 'tag',
        'cache'    => $cache,
        'status'   => $status,
        'id'       => $value,
    ];
    return model('common/Term','loglic')->get($args);;
}

/**
 * 按别名获取标签信息
 * @version 1.0.1 首次引入
 * @param string $value 必需;别名值;默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed 查询结果(array|null)
 */
function videoTagSlug($value='', $cache=true, $status='normal')
{
    $args = [
        'module'   => 'video',
        'controll' => 'tag',
        'cache'    => $cache,
        'status'   => $status,
        'slug'     => $value,
    ];
    return model('common/Term','loglic')->get($args);
}

/**
 * 按名称获取标签信息
 * @version 1.0.1 首次引入
 * @param string $value 必需;分类名称;默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed 查询结果(array|null)
 */
function videoTagName($value='', $cache=true, $status='normal')
{
    $args = [
        'module'   => 'video',
        'controll' => 'category',
        'cache'    => $cache,
        'status'   => $status,
        'name'     => $value,
    ];
    return model('common/Term','loglic')->get($args);
}

/**
 * 快速获取多个热门标签列表的某一个字段
 * @version 1.0.1 首次引入
 * @param int $limit 必需;数量限制；默认：10
 * @param string $field 可选;返回字段;默认：term_name
 * @return mixed 查询结果(array|null)
 */
function videoTags($limit=10, $field='term_name')
{
    return array_column(videoTagSelect([
        'status'=> 'normal',
        'limit' => $limit,
        'sort'  => 'term_count desc,term_id',
        'order' => 'desc',
    ]), $field);
}

/**
 * 通过标签名获取标签ID(所有应用共用一个标签)
 * @version 1.0.1 首次引入
 * @param mixed $tagName 必需;标签名;默认：空
 * @param bool $autoSave可选;是否自动新增;默认：false
 * @return array 查询结果
 */
function videoTagAuto($name=[], $autoSave=false){
    return model('common/Term','loglic')->nameToId($name, 'video', 'tag', $autoSave);
}

/**
 * 按条件获取前贤台导航菜单列表
 * @version 1.0.1 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $result 可选;返回状态(array|tree|level);默认：tree
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称(navbar|navs);默认：空
 *     @type int $limit 可选;分页大小;默认：0
 *     @type string $sort 可选;排序字段名;默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed 查询结果obj|null
 */
function videoNavsSelect($args=[])
{
    return model('common/Navs','loglic')->select($args);
}

/**
 * 按ID快速获取一条视频
 * @version 1.0.1 首次引入
 * @param int $value 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed 查询结果(obj|null)
 */
function videoGetId($value='', $cache=true, $status='normal')
{
    if (!$value) {
        return null;
    }
    return videoGet([
        'cache'  => $cache,
        'status' => $status,
        'id'     => $value,
    ]);
}

/**
 * 按SLUG快速获取一条视频
 * @version 1.0.1 首次引入
 * @param int $value 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed 查询结果(obj|null)
 */
function videoGetSlug($value='', $cache=true, $status='normal')
{
    if (!$value) {
        return null;
    }
    return videoGet([
        'cache'  => $cache,
        'status' => $status,
        'slug'   => $value,
    ]);
}

/**
 * 按NAME快速获取一条视频
 * @version 1.0.1 首次引入
 * @param int $value 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed 查询结果(obj|null)
 */
function videoGetName($value='', $cache=true, $status='normal')
{
    if (!$value) {
        return null;
    }
    return videoGet([
        'cache'  => $cache,
        'status' => $status,
        'name'   => $value,
    ]);
}

/**
 * 按ID快速获取前N个视频
 * @version 1.0.1 首次引入
 * @param int $id 必需;Id值；默认：空
 * @return mixed 查询结果(obj|null)
 */
function videoPrev($id=1, $limit=1)
{
    return videoSelect(['status'=>'normal','limit'=>$limit,'id'=>['lt',$id],'sort'=>'info_id','order'=>'desc']);
}

/**
 * 按ID快速获取后N个视频
 * @version 1.0.1 首次引入
 * @param int $id 必需;Id值；默认：空
 * @return mixed 查询结果(obj|null)
 */
function videoNext($id=1, $limit=1)
{
    return videoSelect(['status'=>'normal','limit'=>$limit,'id'=>['gt',$id],'sort'=>'info_id','order'=>'asc']);
}

/**
 * 添加一个视频
 * @version 1.0.1 首次引入
 * @param array $post 必需;数组格式,支持的字段列表请参考手册
 * @param bool $autoSave 可选;当分类与标签不存在时是否自动新增;默认:false
 * @return int 返回自增ID或0
 */
function videoSave($post=[], $autoSave=false)
{
    $post = videoDataSet($post, $autoSave);
    
    config('common.validate_name','video/Detail');
        
    config('common.validate_scene','save');

    config('common.where_slug_unique',['info_module'=>['eq','video']]);
    
    config('custom_fields.info_meta',videoMetaKeys($post['info_controll'],NULL));

    return \daicuo\Info::save($post, 'info_meta,term_map');
}

/**
 * 按ID删除一条或多条视频
 * @version 1.0.1 首次引入
 * @param mixed $ids 必需;多个用逗号分隔或使用数组传入(array|string);默认：空 
 * @return array ID作为键名,键值为删除结果(bool)
 */
function videoDelete($ids=[])
{
    return model('common/Info','loglic')->deleteIds($ids);
}

/**
 * 修改一个视频(需传入主键值作为更新条件)
 * @version 1.0.1 首次引入
 * @param array $post 必需;表单字段 {
 *     @type int $info_id 必需;按ID修改;默认：空
 * }
 * @param bool $autoSave 可选;当分类与标签不存在时是否自动新增;默认:false
 * @return mixed 成功时返回obj,失败时null
 */
function videoUpdate($post=[], $autoSave=false)
{
    $post = videoDataSet($post, $autoSave);
    
    config('common.validate_name','video/Detail');
        
    config('common.validate_scene','update');

    config('common.where_slug_unique',['info_module'=>['eq','video']]);
    
    config('custom_fields.info_meta',videoMetaKeys($post['info_controll'],NULL));
    
    return \daicuo\Info::update_id($post['info_id'], $post, 'info_meta,term_map');
}

/**
 * 按条件查询多条视频
 * @version 1.0.1 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $result 可选;返回结果类型(array|obj);默认：array
 *     @type string $field 可选;查询字段;默认：*
 *     @type string $status 可选;显示状态（normal|hidden|private）;默认：空
 *     @type string $limit 可选;分页大小;默认：0
 *     @type string $page 可选;当前分页;默认：0
 *     @type string $sort 可选;排序字段名(info_id|info_order|info_views|info_hits|meta_value_num);默认：info_id
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $search 可选;搜索关键词（info_name|info_slug|info_excerpt）;默认：空
 *     @type mixed $id 可选;内容ID限制条件(int|array);默认：空
 *     @type mixed $title 可选;标题限制条件(stirng|array);默认：空
 *     @type mixed $name 可选;名称限制条件(stirng|array);默认：空
 *     @type mixed $slug 可选;别名限制条件(stirng|array);默认：空
 *     @type mixde $action 可选;所属操作名(stirng|array);默认：空
 *     @type mixde $controll 可选;所属控制器(stirng|array);默认：空
 *     @type mixed $term_id 可选;分类法ID限制条件(string|array);默认：空
 *     @type array $meta_query 可选;自定义字段(二维数组[key=>['eq','key'],value=>['in','key']]);默认：空
 *     @type array $with 可选;自定义关联查询条件;默认：空
 *     @type array $view 可选;自定义视图查询条件;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed 查询结果（array|null）
 */
function videoSelect($args=[])
{
    $args = DcArrayArgs($args,[
        'cache'    => true,
        'result'   => 'array',
        'module'   => 'video',
        'with'     => 'info_meta,term,user',
        'view'     => [],
        'field'    => 'info_name,info_slug,info_excerpt,info_create_time,info_update_time,info_parent,info_order,info_user_id,info_type,info_status,info_views,info_hits,info_module,info_controll,info_action,info_title,info_keywords,info_description',
    ]);
    //动态生成视图查询或关联查询条件
    if($args['meta_query']){
        if(config('database.type') == 'mysql'){
            $args['field'].=',info.info_id';
        }else{
            $args['field'] = 'info.*';
        }
    }else{
        if( $args['term_id'] ){
            array_push($args['view'],['term_map' , NULL, 'term_map.detail_id=info.info_id']);
        }
        if($args['meta_key'] || $args['meta_value']){
            array_push($args['view'],['info_meta', NULL, 'info_meta.info_id=info.info_id']);
        }
        if($args['view']){
            $args['field'].=',info.info_id';
            array_push($args['view'],['info', NULL]);
        }else{
            $args['field'].=',info_id';
        }
    }
    //调用查询接口
    return model('common/Info','loglic')->select($args);
}

/**
 * 按条件查询一条视频
 * @version 1.0.1 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type mixed $id 可选;内容ID(stirng|array);默认：空
 *     @type mixed $name 可选;内容名称(stirng|array);默认：空
 *     @type mixed $slug 可选;内容别名(stirng|array);默认：空
 *     @type mixed $title 可选;内容别名(stirng|array);默认：空
 *     @type mixed $user_id 可选;用户ID(stirng|array);默认：空
 *     @type array $with 可选;自定义关联查询条件;默认：空
 *     @type array $view 可选;自定义视图查询条件;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed 查询结果（array|null）
 */
function videoGet($args)
{
    $args = DcArrayArgs($args,[
        'cache'    => true,
        'module'   => 'video',
    ]);
    
    $args = DcArrayEmpty($args);
    
    return videoDataGet( model('common/Info','loglic')->get($args)  );
}

/**
 * 数据修改器（表单处理）
 * @version 1.0.1 首次引入
 * @param array $post 必需;数组格式,支持的字段列表请参考手册;默认:空
 * @param bool $autoSave 可选;当分类与标签不存在时是否自动新增;默认:false
 * @return array 处理后的数据
 */
function videoDataSet($post=[], $autoSave=false){
    //分类处理（category_name、category_id可自动合并）
    $post['term_id'] = [];
    if($post['category_id']){
        $post['term_id'] = DcArrayArgs($post['category_id'], $post['term_id']);
    }
    if($post['category_name']){
        $post['term_id'] = DcArrayArgs(videoCategoryAuto($post['category_name'], $autoSave), $post['term_id']);
    }
    //标签处理（tag_name、tag_id可自动合并）
    if($post['tag_id']){
        $post['term_id'] = DcArrayArgs($post['tag_id'], $post['term_id']);
    }
    if($post['tag_name']){
        $post['term_id'] = DcArrayArgs(videoTagAuto($post['tag_name'], $autoSave), $post['term_id']);
    }
    //别名处理
    if(config('video.slug_first') && !$post['info_slug']){
        $post['info_slug'] = \daicuo\Pinyin::get($post['info_name'], true);
    }
    //首字母
    if(!$post['video_letter']){
        $post['video_letter'] = substr($post['info_slug'], 0, 1);
    }
    //去除不需要的字段
    unset($post['category_name']);
    unset($post['category_id']);
    unset($post['tag_name']);
    unset($post['tag_id']);
    //返回结果
    return DcArrayArgs($post,[
        'info_module'   => 'video',
        'info_controll' => 'detail',
        'info_action'   => 'index',
        'info_staus'    => 'normal',
        'info_type'     => 'index',
        'info_user_id'  => 1,
        'info_excerpt'  => '',
        'video_head'    => 0,
        'video_hot'     => 0,
        'video_up'      => 0,
        'video_down'    => 0,
        'video_letter'  => 0,
    ]);
}

/**
 * 数据获取器
 * @version 1.0.1 首次引入
 * @param array $data 必需;数组格式,支持的字段列表请参考手册;默认:空
 * @return array 处理后的数据
 */
function videoDataGet($data=[]){
    return $data;
}

/**
 * 只获取模块的所有动态扩展字段KEY
 * @version 1.0.1 首次引入
 * @param string $controll 可选;控制器；默认:category
 * @param string $action 可选;操作名；默认:system
 * @return array 二维数组
 */
function videoMetaKeys($controll='detail', $action='index')
{
    $args = [];
    $args['module']   = 'video';
    $args['controll'] = $controll;
    $args['action']   = $action;
    $keys = model('common/Field','loglic')->forms(DcArrayEmpty($args),'keys');
    return array_unique($keys);
}

/**
 * 获取模块的所有动态扩展字段列表
 * @version 1.0.1 首次引入
 * @param string $controll 可选;控制器；默认:category
 * @param string $action 可选;操作名；默认:system
 * @return array 二维数组
 */
function videoMetaList($controll='detail', $action='index')
{
    $args = [];
    $args['module']   = 'video';
    $args['controll'] = $controll;
    $args['action']   = $action;
    return model('common/Field','loglic')->forms( DcArrayEmpty($args) );
}

/**
 * 根据地址栏参数的扩展字段生成多条件查询参数
 * @version 1.0.1 首次引入
 * @param array $query 必需;地址栏请求参数;默认：空
 * @return array 适用于模型查询函数的meta_query选项
 */
function videoMetaQuery($query=[])
{
    return DcMetaQuery(videoMetaList('detail',NULL), $query);
}

/**
 * 获取内容模型所有扩展字段（初始扩展+自定义扩展）
 * @version 1.0.1 首次引入
 * @return array 文章模型的扩展字段
 */
function videoMetaFields()
{
    //解析自定义字段
    $plus = array_keys( json_decode(config('video.info_meta'),true) );
    //文章初始扩展字段
    return DcArrayArgs($plus, config('video.meta_detail'));
}

/**
 * 表格筛选选项处理
 * @version 1.0.1 首次引入
 * @return array 处理后的数据
 */
function videoDataOption($options=[]){
    return array_merge($options,[''=>'---']);
}

/**
 * 内容模型基础字段计数增加
 * @version 1.0.0 首次引入
 * @param int $id 必需;ID值;默认:空
 * @param string $field 必需;字段值;默认:info_views
 * @param int $numb 可选;步进值;默认:1
 * @param int $time 可选;延迟更新;默认:0
 * @return int 最新值
 */
function videoInfoInc($id=0, $field='info_views', $num=1, $time=0){
    if(!$id){
        return 0;
    }
    return dbUpdateInc('common/Info', ['info_id'=>['eq',$id]], $field, $num, $time);
}

/**
 * 内容模型扩展字段计数增加
 * @version 1.0.1 首次引入
 * @param int $id 必需;ID值;默认:空
 * @param string $field 必需;字段值;默认:info_views
 * @param int $numb 可选;步进值;默认:1
 * @param int $time 可选;延迟更新;默认:0
 * @return int 最新值
 */
function videoMetaInc($id=0, $field='video_up', $num=1, $time=0){
    if(!$id){
        return 0;
    }
    return dbUpdateInc('common/infoMeta', ['info_id'=>['eq',$id],'info_meta_key'=>['eq',$field]], 'info_meta_value', $num, $time);
}

/**
 * 生成分类页链接
 * @version 1.0.1 首次引入
 * @param array $info 必需;[id,name,slug]；默认:空
 * @param mixed $pageNumber 可选;int|[PAGE];默认:空
 * @return string 生成的内部网址链接
 */
function videoUrlCategory($info=[], $pageNumber='')
{
    $route = config('video.rewrite_category');
    $args = [];
    if( preg_match('/:slug|<slug/i',$route) ){
        $args['slug'] = $info['term_slug'];
    }elseif( preg_match('/:name|<name/i',$route) ){
        $args['name'] = $info['term_name'];
    }else{
        $args['id'] = $info['term_id'];
    }
    if($pageNumber){
        $args['pageNumber'] = $pageNumber;
    }
    return DcUrl('video/category/index', $args);
}

/**
 * 生成标签页链接
 * @version 1.0.1 首次引入
 * @param array $info 必需;[id,name,slug]；默认:空
 * @param mixed $pageNumber 可选;int|[PAGE];默认:空
 * @return string 生成的内部网址链接
 */
function videoUrlTag($info=[], $pageNumber='')
{
    //伪静态规则
    $route = config('video.rewrite_tag');
    //URL链接参数
    $args  = [];
    if( preg_match('/:slug|<slug/i',$route) ){
        $args['slug'] = $info['term_slug'];
    }elseif( preg_match('/:name|<name/i',$route) ){
        $args['name'] = $info['term_name'];
    }else{
        $args['id'] = $info['term_id'];
    }
    //分页参数
    if($pageNumber){
        $args['pageNumber'] = $pageNumber;
    }
    return DcUrl('video/tag/index', DcArrayEmpty($args));
}

/**
 * 生成搜索页链接
 * @version 1.0.1 首次引入
 * @param string $action 必须;操作名;默认:index
 * @param array $args 可选;['searchText','pageNumber','pageSize','sortName','sortOrder'];默认:空
 * @return string 生成的内部网址链接
 */
function videoUrlSearch($path='video/search/index', $args=[])
{
    return DcUrl($path, DcArrayFilter($args, ['searchText','pageNumber']));
}

/**
 * 生成详情页链接
 * @version 1.0.1 首次引入
 * @param array $info 必需;[id,name,slug]；默认：空
 * @param mixed $pageNumber 可选;int|[PAGE];默认:空
 * @return string 生成的内部网址链接
 */
function videoUrlDetail($info=[], $pageNumber='')
{
    $route = config('video.rewrite_detail');
    $args = [];
    //必要参数
    if( preg_match('/:slug|<slug/i',$route) ){
        $args['slug'] = $info['info_slug'];
    }elseif( preg_match('/:name|<name/i',$route) ){
        $args['name'] = $info['info_name'];
    }else{
        $args['id'] = $info['info_id'];
    }
    //分类参数
    if( preg_match('/:termSlug|<termSlug/i',$route) ){
        $args['termSlug'] = $info['category_slug'][0];
    }
    if( preg_match('/:termId|<termId/i',$route) ){
        $args['termId'] = $info['category_id'][0];
    }
    if( preg_match('/:termName|<termName/i',$route) ){
        $args['termName'] = $info['category_name'][0];
    }
    return DcUrl('video/detail/index', $args);
}

/**
 * 生成图片附件链接
 * @version 1.0.1 首次引入
 * @param string $file 必需;图片传件路径;默认：空
 * @param string $root 可选;根目录;默认：/
 * @param string $default 可选;默认图片地址;默认：x.gif
 * @return string 无图片时返回默认横向图
 */
function videoUrlImage($file='', $root='/', $default='public/images/x.gif')
{
    return DcEmpty(DcUrlAttachment($file), $root.$default);
}

/**
 * 导航菜单高亮属性
 * @version 1.0.1 首次引入
 * @param string $path 请求路径
 * @param string $termId 分类ID验证
 * @param string $navActive 预设active值
 * @param string $navId 菜单ID
 * @return string active或空
 */
function videoNavActive($path='', $termId=0, $navActive='', $navId=0)
{
    if($termId){
        if($navId == $termId){
            return 'active';
        }
    }else{
        if($path == $navActive){
            return 'active';
        }
    }
    return '';
}

/**
 * 替换全站搜索引擎关键字
 * @version 1.0.1 首次引入
 * @param string $route 必需;包含待替换的关键字;空
 * @param string $page 可选;页码;空
 * @return string 过滤后的文本
 */
function videoSeo($string='', $page=0)
{
    if(!$string){
        return '欢迎使用呆错短视频系统（DaiCuoVideo）';
    }
    if(!$page){
        $page = 1;
    }
    $search = ['[siteName]', '[siteDomain]', '[pageNumber]'];
    $replace = [config('common.site_name'), config('common.site_domain'), $page];
    return str_replace($search, $replace, videoTrim($string));
}

/**
 * 获取搜索列表
 * @version 1.0.2 
 * @return mixed string|array
 */
function videoSearchList()
{
    return explode(',',config('video.search_list'));
    //return DcArrayArgs(explode(',',config('video.search_list')),['video/search/index']);
}

/**
 * 格式化分页大小
 * @version 1.0.1 首次引入
 * @param string $pageSize 必需;每页大小;默认：10
 * @return intval 页码值
 */
function videoPageSize($pageSize=10)
{
    if($pageSize > 0 && $pageSize < 501){
        return $pageSize;
    }
    return 10;
}

/**
 * 格式化排序字段
 * @version 1.0.1 首次引入
 * @param string $sortName 必需;排序字段；默认:空
 * @param string $sortDefault 必需;默认字段；默认:info_update_time
 * @return string 生成合法的排序字段
 */
function videoSortName($sortName='',$sortDefault='info_update_time')
{
    if( in_array($sortName,['video_up','video_down','info_id','info_order','info_views','info_hits','info_create_time','info_update_time']) ){
        return $sortName;
    }
    return $sortDefault;
}

/**
 * 格式化排序方式
 * @version 1.0.1 首次引入
 * @param string $sortName 必需;排序字段；默认:空
 * @param string $sortDefault 必需;默认字段；默认:desc
 * @return string 生成合法的排序字段
 */
function videoSortOrder($sortOrder='',$sortDefault='desc')
{
    if( in_array($sortOrder,['desc','asc']) ){
        return $sortOrder;
    }
    return $sortDefault;
}

/**
 * 获取视频模型所有字段
 * @version 1.0.1 首次引入
 * @return mixed 成功时返回array,失败时null
 */
function videoFields()
{
    $fields = array_keys(DcFields('info'));
    $fieldsMeta = videoMetaKeys('detail', NULL);
    return DcArrayArgs($fieldsMeta, $fields);
}

/**
 * 字符串截取
 * @version 1.0.0 首次引入
 * @param string $string 必需;待截取的字符串
 * @param int $start 必需;起始位置;默认：0
 * @param int $length 必需;截取长度;默认：420
 * @param bool $suffix 可选;超出长度是否以...显示;默认：true
 * @param string $charset 可选;字符编码;默认：utf-8
 * @return string $string 截取后的字符串
 */
function videoSubstr($string, $start=0, $length=420, $suffix=true, $charset="UTF-8"){
    $string = strip_tags(htmlspecialchars_decode(videoTrim($string)));
    return htmlspecialchars(DcSubstr($string, $start, $length, $suffix, $charset));
}

/**
 * 过滤连续空白
 * @version 1.0.0 首次引入
 * @param string $str 待过滤的字符串
 * @return string 处理后的字符串
 */
function videoTrim($str=''){
    $str = str_replace("　",' ',str_replace("&nbsp;",' ',trim($str)));
    $str = preg_replace('#\s+#', ' ', $str);
    return $str;
}