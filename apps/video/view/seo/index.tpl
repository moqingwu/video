{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("video/seo/index")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  {:lang("video/seo/index")}
</h6>
{:DcBuildForm([
    'name'     => 'video/seo/index',
    'class'    => 'bg-white py-2',
    'action'   => DcUrlAddon(['module'=>'video','controll'=>'seo','action'=>'update']),
    'method'   => 'post',
    'ajax'     => true,
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'disabled' => false,
    'callback' => '',
    'items'    => $fields,
])}
{/block}