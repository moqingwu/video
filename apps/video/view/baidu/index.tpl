{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("video/baidu/index")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">推送内容页</h6>
{:DcBuildForm([
    'name'          => 'video/baidu/index',
    'class'         => 'bg-white py-2 mb-4',
    'action'        => DcUrlAddon(['module'=>'video','controll'=>'baidu','action'=>'push']),
    'method'        => 'post',
    'submit'        => lang('submit'),
    'reset'         => lang('reset'),
    'close'         => false,
    'disabled'      => false,
    'ajax'          => false,
    'callback'      => '',
    'class_tabs'    => 'mb-2',
    'class_link'    => 'rounded-0',
    'class_content' => 'border p-3',
    'class_button'  => 'form-group mb-0',
    'submit_class'  => 'btn btn-sm btn-purple',
    'reset_class'   => 'btn btn-sm btn-dark',
    'items'         => $fields,
])}
<h6 class="border-bottom pb-2 text-purple">手动提交网址</h6>
{:DcBuildForm([
    'name'          => 'video/baidu/index',
    'class'         => 'bg-white py-2',
    'action'        => DcUrlAddon(['module'=>'video','controll'=>'baidu','action'=>'forum']),
    'method'        => 'post',
    'submit'        => lang('submit'),
    'reset'         => lang('reset'),
    'close'         => false,
    'disabled'      => false,
    'ajax'          => false,
    'callback'      => '',
    'class_tabs'    => 'mb-2',
    'class_link'    => 'rounded-0',
    'class_content' => 'border p-3',
    'class_button'  => 'form-group mb-0',
    'submit_class'  => 'btn btn-sm btn-purple',
    'reset_class'   => 'btn btn-sm btn-dark',
    'items'         => $fieldsForm,
])}
{/block}