<?php
namespace app\video\behavior;

use think\Controller;

class Hook extends Controller
{
    //后台统计
    public function adminIndexCount(&$datas)
    {
        array_push($datas,[
            'id'    => 'view',
            'title' => 'video/count/view',
            'ico'   => 'fa-adjust',
            'color' => 'text-info',
            'count' => model('video/Count','loglic')->views(),
        ]);
        array_push($datas,[
            'id'    => 'detail',
            'title' => 'video/count/detail',
            'ico'   => 'fa-file-text',
            'color' => 'text-primary',
            'count' => model('video/Count','loglic')->detail(),
        ]);
        array_push($datas,[
            'id'    => 'category',
            'title' => 'video/count/category',
            'ico'   => 'fa-folder-o',
            'color' => 'text-danger',
            'count' => model('video/Count','loglic')->category(),
        ]);
        array_push($datas,[
            'id'    => 'tag',
            'title' => 'video/count/tag',
            'ico'   => 'fa-tags',
            'color' => 'text-warning',
            'count' => model('video/Count','loglic')->tag(),
        ]);
    }
    
    //前台权限扩展
    public function adminCapsFront(&$caps)
    {
        $caps = array_merge($caps,[
            'video/data/index',
        ]);
    }
    
    //后台权限扩展
    public function adminCapsBack(&$caps)
    {
        $caps = array_merge($caps,[
            'video/admin/index',
            'video/admin/save',
            'video/admin/delete',
            'video/collect/index',
            'video/collect/save',
            'video/collect/delete',
            'video/collect/write',
        ]);
    }
}