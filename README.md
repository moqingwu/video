#### 开源免费
呆错短视频系统（DaiCuoVideo）是一款专业的免费视频管理系统，适合做短视频垂直细分内容平台。

#### 系统稳定
内核安全稳定、PHP+MYSQL/SQLITE架构、跨平台运行。ThinkPhp+Jquery+BootStrap组合、超强负载能力助您轻松运营百万级站点。

#### 三端合一
呆错短视频系统一套模板自适应电脑、手机、平板多个终端入口，完美适配微信、百度等多种APP浏览器，也可以独立设置移动端与电脑端模板分离。

#### 在线演示
- 通过[demo.daicuo.org](https://demo.daicuo.org/video)可在线预览电脑端与移动端前台演示

#### 运行环境
- 支持 PHP 5.4-7.4
- 支持 MySQL、SQLITE 数据库

#### 安装说明
- 通过[www.daicuo.org](https://www.daicuo.org/product/video)下载最新版本上传至网站运行根目录即可使用
- 通过GIT克隆本仓库代码至网站运行环境目录即可使用
- 后台入口（admin.php）、默认用户名（admin）、默认密码（admin888）

#### 联系我们
- 官方网站：[www.daicuo.org](https://www.daicuo.org/product/video)
- 交流论坛：[www.daicuo.co](http://www.daicuo.co)
- 建议反馈：271513820@qq.com

#### 功能模块
- 视频模块
- 分类模块
- 标签模块
- 解析模块
- 聚合模块
- SEO模块
- 采集模块
- API模块
- 搜索模块
- 统计模块
- 地图模块
- 广告模块
- 友链模块
- 文章模块
- 单页模块
- 用户模块
- 权限模块
- 字段模块
- 日志模块
- 菜单模块
- 水印模块
- 升级模块
- 插件模块
- 语言模块
- 缓存模块
- 云存储模块
- 伪静态模块
- 数据库模块
- 自适应模板

#### 参与贡献
1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request